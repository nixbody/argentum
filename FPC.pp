{$CODEPAGE UTF8}
{$MODE OBJFPC}{$H+}
{$MODESWITCH ADVANCEDRECORDS}
{$MODESWITCH ANONYMOUSFUNCTIONS}
{$MODESWITCH FUNCTIONREFERENCES}
{$MODESWITCH IMPLICITFUNCTIONSPECIALIZATION}

{ Following warnings and notes are silenced because of Generics.Collections. }
{$WARN 3057 OFF} { Warning: An inherited method is hidden. }
{$WARN 4046 OFF} { Warning: Constructing a class with abstract methods. }
{$WARN 5071 OFF} { Note: A private type never used. }
{$WARN 6058 OFF} { Note: Call to a subroutine marked as inline is not inlined. }
