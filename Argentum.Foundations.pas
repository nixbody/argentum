unit Argentum.Foundations;

{$I FPC}

interface

uses
  Argentum.Arc, Classes, Dom, FpExprPars, Generics.Collections, TypInfo;

type
  { An array of pointers to information about a class properties. }
  TPropertiesInformation = array of PPropInfo;
  { Represents an interpolated string. }
  TInterpolated = class
  private
    type
      TExpressionList = specialize TObjectList<TFpExpressionParser>;
      TExpressions = specialize TArc<TExpressionList, acCreate>;
      TExpressionsDict = specialize TDictionary<string, TExpressions>;
    class threadvar
      FCache: specialize TArc<TExpressionsDict, acCreate>;
    var
      FContext: TObject;
      FExpressions: TExpressions;
      FKey: string;
      FTemplate: string;
  public
    { Creates a new interpolated string from the given template.
      @param(Template is a string that should be evaluated.)
      @param(Context is an object that holds variables (published properties)
      that can be used in expressions. If not provided then Self.) }
    constructor Create(const Template: string; Context: TObject = nil);
    { Evaluates all the expressions and returns the resulting string.
      @returns(The actual interpolated string with all the expressions
      evaluated.) }
    function AsString: string;
  end;
  { This is the foundation for components. }
  TFoundation = class(TComponent)
  private
    type
      TClasses = specialize TDictionary<string, TClass>;
      TProperty = packed record
      private
        var
          FInfo: PPropInfo;
          FValue: TInterpolated;
      public
        class function Dispense(AInfo: PPropInfo; AValue: TInterpolated):
          TProperty; static; inline;
        procedure Evaluate(Context: TObject);
      end;
      TProperties = specialize TDictionary<string, TProperty>;
    class threadvar
      FClassCache: specialize TArc<TClasses, acCreate>;
    var
      FIf: Boolean;
      FParent: TComponent;
      FProperties: TProperties;
  protected
    { Returns the reference to the class of the given name.
      @param(AClassName is the name of the class to return.)
      @returns(The reference to the class of the given name.) }
    class function ClassNamed(const AClassName: string): TClass; static;
    { Returns a reference to the class representing the given DOM node.
      @returns(A reference to the class representing the given DOM node.) }
    class function ClassOfDomNode(Node: TDomNode): TClass; virtual;
    { Returns the name of DOM node that represents this class.
      @returns(The name of DOM node that represents this class.) }
    class function DomName: string; virtual;
    { Returns information about published properties of the given class.
      @param(AClass is the reference to the class of which published properties
      information should be returned.)
      @returns(Information about published properties of the given class.) }
    class function PublishedPropertiesOf(AClass: TClass):
      TPropertiesInformation; static;
    { Tries to find the reference to the class of the given name.
      @param(AClassName is the name of the class to find.)
      @param(Found is the reference to the class of the given name.)
      @returns(@true if the class has been found, @false otherwise.) }
    class function TryClassNamed(const AClassName: string; out Found: TClass):
      Boolean; static;
    { Binds the given expression to the given published property.
      @param(Prop is the name of the published property the expression should be
      bound to.)
      @param(Template is a string from which the expression should be built.) }
    procedure SetProperty(const Prop, Template: string); virtual;
  public
    { Creates a new component that can be based on the given DOM template.
      @param(AOwner is the owner of that newly created component.)
      @param(Template is a DOM node that should be used as a template for that
      newly created component.) }
    constructor Create(AOwner: TComponent; Template: TDomNode = nil); virtual;
    { The destructor does the clean-up when the component is being destroyed. }
    destructor Destroy; override;
    { Evaluates properties of this component and all its descendants. }
    procedure EvaluateProperties;
    { Returns the parent component of this component.
      @returns(The parent component of this component.) }
    function GetParentComponent: TComponent; override;
    { Tells whether or not this component has a parent.
      @returns(@true if this component has a parent, @false otherwise.) }
    function HasParent: Boolean; override;
    { Renders this component to the given DOM.
      @param(Dom is that DOM this component should be rendered to.) }
    procedure Render(Dom: TDomNode); virtual;
    { Binds the given expression to the given published property. }
    property Properties[const Prop: string]: string write SetProperty; default;
  published
    { Tells whether or not this component should be rendered. }
    property AgIf: Boolean read FIf write FIf;
    { The parent component of this component. }
    property Parent: TComponent read GetParentComponent;
  end;
  { A block of components. }
  TAgBlock = class(TFoundation)
  end;
  { Generic text component. }
  TTextContent = class(TFoundation)
  private
    var
      FText: string;
  public
    { Creates a new component that can be based on the given DOM template.
      @param(AOwner is the owner of that newly created component.)
      @param(Template is a DOM node that should be used as a template for that
      newly created component.) }
    constructor Create(AOwner: TComponent; Template: TDomNode = nil); override;
    { Renders this component to the given DOM.
      @param(Dom is that DOM this component should be rendered to.) }
    procedure Render(Dom: TDomNode); override;
  published
    { The text content of this component. }
    property AsString: string read FText write FText;
  end;

implementation

uses
  RegExpr, StrUtils, SysUtils;

constructor TInterpolated.Create(const Template: string; Context: TObject);
var
  CachedExpressions: TExpressions;
  Pattern: TRegExpr;
  Properties: TPropertiesInformation;
procedure AddExpr;
var
  Prop: PPropInfo;
begin
  with FExpressions._ do
    with Pattern do
    begin
      Add(TFpExpressionParser.Create(nil));
      with Last do
      begin
        BuiltIns := [Low(TBuiltInCategory)..High(TBuiltInCategory)];
        for Prop in Properties do
          with Prop^ do
            with Identifiers do
              case PropType^.Kind of
                tkAString, tkChar, tkEnumeration, tkLString, tkSString,
                tkUString, tkWString:
                  AddStringVariable(Name, '');
                tkBool:
                  AddBooleanVariable(Name, False);
                tkClass, tkClassRef, tkInt64, tkInteger, tkPointer, tkQWord:
                  AddIntegerVariable(Name, 0);
                tkFloat:
                  AddFloatVariable(Name, 0.0)
                otherwise
                  if PropType^.Name = 'TDateTime' then
                    AddDateTimeVariable(Name, 0.0)
              end;
        Expression := Match[1]
      end
    end
end;
begin
  inherited Create;
  if not Assigned(Context) then Context := Self;
  FKey := Template + '@' + PtrUInt(Context.ClassType).ToString;
  with FCache._ do
    if not TryGetValue(FKey, CachedExpressions) then
    begin
      FExpressions._.OwnsObjects := True;
      Pattern := TRegExpr.Create('\$\{(.+?)\}');
      Properties := TFoundation.PublishedPropertiesOf(Context.ClassType);
      with Pattern do
        try
          if Exec(Template) then
          begin
            AddExpr;
            while ExecNext do AddExpr
          end
        finally
          Free
        end;
      CachedExpressions := FExpressions;
      AddOrSetValue(FKey, CachedExpressions)
    end;
  FContext := Context;
  FTemplate := Template;
  FExpressions := CachedExpressions;
end;

function TInterpolated.AsString: string;
var
  Expr: TFpExpressionParser;
  I: Word;
  Properties: TPropertiesInformation;
  Res: TFpExpressionResult;
begin
  Result := FTemplate;
  Properties := TFoundation.PublishedPropertiesOf(FContext.ClassType);
  for Expr in FExpressions._ do
  begin
    for I := 0 to Expr.Identifiers.Count - 1 do
      with TFpExprIdentifierDef(Expr.Identifiers[I]) do
        case ResultType of
          rtBoolean: AsBoolean := GetOrdProp(FContext, Properties[I]) <> 0;
          rtCurrency: AsCurrency := GetFloatProp(FContext, Properties[I]);
          rtDateTime: AsDateTime := GetFloatProp(FContext, Properties[I]);
          rtFloat: AsFloat := GetFloatProp(FContext, Properties[I]);
          rtInteger: AsInteger := GetOrdProp(FContext, Properties[I]);
          rtString:
            case Properties[I]^.PropType^.Kind of
              tkChar: AsString := Char(GetOrdProp(FContext, Properties[I]));
              tkEnumeration: AsString := GetEnumProp(FContext, Properties[I])
              otherwise AsString := GetStrProp(FContext, Properties[I])
            end
        end;
    with Expr do
    begin
      Res := Evaluate;
      with Res do
        case ResultType of
          rtString:
            Result := Result.Replace('${' + Expression  + '}', ResString)
          otherwise
            Result := Result.Replace('${' + Expression  + '}',
              ArgToFloat(Res).ToString)
        end
    end
  end
end;

class function TFoundation.TProperty.Dispense(AInfo: PPropInfo;
  AValue: TInterpolated): TProperty;
begin
  with Result do
  begin
    FInfo := AInfo;
    FValue := AValue
  end
end;

procedure TFoundation.TProperty.Evaluate(Context: TObject);
var
  Val: string;
begin
  Val := FValue.AsString;
  case FInfo^.PropType^.Kind of
    tkAString, tkLString, tkSString, tkUString, tkWString:
      SetStrProp(Context, FInfo, Val);
    tkBool:
      SetOrdProp(Context, FInfo, Int64(Val.ToBoolean));
    tkChar:
      if Val <> '' then
        SetOrdProp(Context, FInfo, Int64(Val[1]))
      else
        SetOrdProp(Context, FInfo, 0);
    tkClass, tkClassRef, tkInt64, tkInteger, tkPointer, tkQWord:
      SetOrdProp(Context, FInfo, Val.ToInt64);
    tkEnumeration:
      SetEnumProp(Context, FInfo, Val);
    tkFloat:
      SetFloatProp(Context, FInfo, Val.ToExtended)
    otherwise;
  end
end;

class function TFoundation.ClassNamed(const AClassName: string): TClass;
begin
  if not FClassCache._.TryGetValue(AClassName, Result) then
  begin
    Result := FindClass(AClassName);
    FClassCache._.AddOrSetValue(AClassName, Result)
  end
end;

class function TFoundation.ClassOfDomNode(Node: TDomNode): TClass;
begin
  with Node do
    case NodeType of
      CDATA_SECTION_NODE, TEXT_NODE:
        Result := TTextContent;
      ELEMENT_NODE:
        Result := ClassNamed('T' + StringsReplace(string(NodeName), ['-', ':'],
          ['', ''], [rfReplaceAll]))
      otherwise
        Result := TFoundation
    end
end;

class function TFoundation.DomName: string;
begin
  Result := ClassName.Substring(1)
end;

class function TFoundation.PublishedPropertiesOf(AClass: TClass):
  TPropertiesInformation;
var
  I, PropCount: Word;
  Props: PPropList = nil;
begin
  Result := nil;
  PropCount := GetTypeData(AClass.ClassInfo)^.PropCount;
  GetMem(Props, PropCount * SizeOf(Props^[0]));
  GetPropInfos(AClass.ClassInfo, Props);
  SetLength(Result, PropCount);
  for I := 0 to PropCount - 1 do Result[I] := Props^[I];
  FreeMem(Props);
end;

class function TFoundation.TryClassNamed(const AClassName: string;
  out Found: TClass): Boolean;
begin
  if not FClassCache._.TryGetValue(AClassName, Found) then
  begin
    Found := GetClass(AClassName);
    if Assigned(Found) then
    begin
      FClassCache._.AddOrSetValue(AClassName, Found);
      Result := True
    end
    else
      Result := False
  end
  else
    Result := True
end;

constructor TFoundation.Create(AOwner: TComponent; Template: TDomNode);
type
  TFoundationClass = class of TFoundation;
var
  I: Integer;
begin
  inherited Create(AOwner);
  FIf := True;
  FProperties := TProperties.Create;
  if (Assigned(AOwner)) and (AOwner.HasParent) then
    FParent := AOwner.GetParentComponent
  else
    FParent := Self;
  if Assigned(Template) then
    with Template do
    begin
      with Attributes do
        for I := 0 to Length - 1 do
          with Item[I] do
            if NodeName.EndsWith(':') then
              Self[string(NodeName)] := '${' + string(NodeValue) + '}'
            else
              Self[string(NodeName)] := string(NodeValue);
      with ChildNodes do
        for I := 0 to Length - 1 do
          with Item[I] do
            if (NodeType <> TEXT_NODE) or (TextContent.Trim <> '') then
              TFoundationClass(ClassOfDomNode(Item[I])).Create(Self, Item[I])
    end
end;

destructor TFoundation.Destroy;
var
  Prop: TProperty;
begin
  for Prop in FProperties.Values do Prop.FValue.Free;
  FProperties.Free;
  inherited
end;

procedure TFoundation.SetProperty(const Prop, Template: string);
var
  Ctx: TObject;
  Key: string;
  Stored: TProperty;
begin
  if HasParent then Ctx := GetParentComponent else Ctx := Self;
  Key := StringsReplace(Prop, ['-', ':'], ['', ''], [rfReplaceAll]).ToLower;
  if FProperties.TryGetValue(Key, Stored) then
    with Stored do
      if (FValue.FTemplate = Template) and
        (FValue.FContext.ClassType = Ctx.ClassType) then
        FValue.FContext := Ctx
      else
      begin
        FValue.Free;
        FValue := TInterpolated.Create(Template, Ctx);
        FProperties.AddOrSetValue(Key, Stored)
      end
  else
    FProperties.AddOrSetValue(Key, TProperty.Dispense(FindPropInfo(Self, Key),
      TInterpolated.Create(Template, Ctx)))
end;

procedure TFoundation.EvaluateProperties;
var
  Owned: TComponent;
  Prop: TProperty;
begin
  for Prop in FProperties.Values do Prop.Evaluate(Self);
  for Owned in Self do (Owned as TFoundation).EvaluateProperties
end;

function TFoundation.GetParentComponent: TComponent;
begin
  Result := FParent
end;

function TFoundation.HasParent: Boolean;
begin
  Result := Assigned(FParent)
end;

procedure TFoundation.Render(Dom: TDomNode);
var
  Element: TDomElement;
  Owned: TComponent;
  Prop: PPropInfo;
  Val: string;
begin
  if FIf then
  begin
    if Dom is TDomDocument then
      Element := TDomDocument(Dom).CreateElement(Utf8Decode(DomName.ToLower))
    else
      Element := Dom.OwnerDocument.CreateElement(Utf8Decode(DomName.ToLower));
    Dom.AppendChild(Element);
    for Prop in PublishedPropertiesOf(ClassType) do
      with Prop^ do
        if not Name.StartsWith('Ag') or (Name.Length <= 2) or
          not (Name[3] in ['A'..'Z']) then
        begin
          case PropType^.Kind of
            tkAString, tkLString, tkSString, tkUString, tkWString:
              Val := GetStrProp(Self, Prop);
            tkBool:
              Val := IfThen(GetOrdProp(Self, Prop) <> 0, Name, '');
            tkChar:
              Val := Char(GetOrdProp(Self, Prop));
            tkInt64, tkInteger, tkQWord:
              Val := GetOrdProp(Self, Prop).ToString;
            tkEnumeration:
              Val := GetEnumProp(Self, Prop);
            tkFloat:
              Val := GetFloatProp(Self, Prop).ToString
            otherwise
              Val := ''
          end;
          if Val <> '' then
            Element.SetAttribute(Utf8Decode(Name.ToLower), Utf8Decode(Val))
        end;
    for Owned in Self do (Owned as TFoundation).Render(Element)
  end
end;

constructor TTextContent.Create(AOwner: TComponent; Template: TDomNode);
begin
  inherited Create(AOwner);
  if Assigned(Template) then Self['asstring'] := string(Template.TextContent)
end;

procedure TTextContent.Render(Dom: TDomNode);
var
  Text: TDOMText;
begin
  if Dom is TDomDocument then
    Text := TDomDocument(Dom).CreateTextNode(Utf8Decode(FText))
  else
    Text := Dom.OwnerDocument.CreateTextNode(Utf8Decode(FText));
  Dom.AppendChild(Text)
end;

initialization

SetMultiByteConversionCodePage(CP_UTF8);
RegisterClasses([TAgBlock])

end.
