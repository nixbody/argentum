unit Argentum.Html.Components;

{$I FPC}

interface

uses
  Argentum.Foundations, Dom, Generics.Collections;

type
  { The foundation of all HTML components. }
  THtmlComponentFoundation = class abstract(TFoundation)
  protected
    { Returns a reference to the class representing the given DOM node.
      @returns(A reference to the class representing the given DOM node.) }
    class function ClassOfDomNode(Node: TDomNode): TClass; override;
    { Binds the given expression to the given published property.
      @param(Prop is the name of the published property the expression should be
      bound to.)
      @param(Template is a string from which the expression should be built.) }
    procedure SetProperty(const Prop, Template: string); override;
  end;
  { This is the base class for all the HTML components. }
  THtmlComponent = class abstract(THtmlComponentFoundation)
  protected
    { Returns the name of DOM node that represents this class.
      @returns(The name of DOM node that represents this class.) }
    class function DomName: string; override;
  public
    { Renders this component to the given DOM.
      @param(Dom is that DOM this component should be rendered to.) }
    procedure Render(Dom: TDomNode); override;
  end;
  { A block of components. }
  TAgBlockHtmlComponent = class(THtmlComponent)
  end;
  { This is the base class for non-custom HTML elements. }
  TNonCustomHtmlComponent = class abstract(THtmlComponentFoundation)
  private
    var
      FAccessKey: string;
      FAutoCapitalize: string;
      FAutofocus: Boolean;
      FContentEditable: string;
      FClass: string;
      FDir: string;
      FDraggable: string;
      FEnterKeyHint: string;
      FExportParts: string;
      FHidden: string;
      FId: string;
      FInert: Boolean;
      FInputMode: string;
      FIsCustom: string;
      FItemId: string;
      FItemProp: string;
      FItemRef: string;
      FItemScope: Boolean;
      FItemType: string;
      FLang: string;
      FNOnce: string;
      FOnAbort: string;
      FOnAutocomplete: string;
      FOnAutocompleteError: string;
      FOnBlur: string;
      FOnCancel: string;
      FOnCanPlay: string;
      FOnCanPlaythrough: string;
      FOnChange: string;
      FOnClick: string;
      FOnClose: string;
      FOnContextMenu: string;
      FOnCueChange: string;
      FOnDblClick: string;
      FOnDrag: string;
      FOnDragEnd: string;
      FOnDragEnter: string;
      FOnDragLeave: string;
      FOnDragOver: string;
      FOnDragStart: string;
      FOnDrop: string;
      FOnDurationChange: string;
      FOnEmptied: string;
      FOnEnded: string;
      FOnError: string;
      FOnFocus: string;
      FOnInput: string;
      FOnInvalid: string;
      FOnKeyDown: string;
      FOnKeyPress: string;
      FOnKeyUp: string;
      FOnLoad: string;
      FOnLoadedData: string;
      FOnLoadedMetaData: string;
      FOnLoadStart: string;
      FOnMouseDown: string;
      FOnMouseEnter: string;
      FOnMouseLeave: string;
      FOnMouseMove: string;
      FOnMouseOut: string;
      FOnMouseOver: string;
      FOnMouseUp: string;
      FOnMouseWheel: string;
      FOnPause: string;
      FOnPlay: string;
      FOnPlaying: string;
      FOnProgress: string;
      FOnRateChange: string;
      FOnReset: string;
      FOnResize: string;
      FOnScroll: string;
      FOnSeeked: string;
      FOnSeeking: string;
      FOnSelect: string;
      FOnShow: string;
      FOnSort: string;
      FOnStalled: string;
      FOnSubmit: string;
      FOnSuspend: string;
      FOnTimeUpdate: string;
      FOnToggle: string;
      FOnVolumeChange: string;
      FOnWaiting: string;
      FPart: string;
      FPopover: string;
      FRole: string;
      FSlot: string;
      FSpellCheck: string;
      FStyle: string;
      FTabIndex: string;
      FTitle: string;
      FTranslate: string;
      FVirtualKeyboardPolicy: string;
      FXmlBase: string;
      FXmlLang: string;
      FXmlNs: string;
  protected
    { Returns the name of DOM node that represents this class.
      @returns(The name of DOM node that represents this class.) }
    class function DomName: string; override;
  public
    { Renders this component to the given DOM.
      @param(Dom is that DOM this component should be rendered to.) }
    procedure Render(Dom: TDomNode); override;
  published
    { Keyboard shortcut to activate or add focus to the element. }
    property AccessKey: string read FAccessKey write FAccessKey;
    { Sets whether input is automatically capitalised when entered by user. }
    property AutoCapitalize: string read FAutoCapitalize write FAutoCapitalize;
    { Indicates that an element is to be focused on page load, or as soon as the
      <dialog> it is part of is displayed. This attribute is a boolean,
      initially false. }
    property Autofocus: Boolean read FAutofocus write FAutofocus;
    { Indicates whether the element's content is editable. }
    property ContentEditable: string read FContentEditable
      write FContentEditable;
    { Often used with CSS to style elements with common properties. }
    property CssClass: string read FClass write FClass;
    { Defines the text direction. Allowed values are ltr (Left-To-Right) or
      rtl (Right-To-Left). }
    property Dir: string read FDir write FDir;
    { Defines whether the element can be dragged. }
    property Draggable: string read FDraggable write FDraggable;
    { The enterkeyhint specifies what action label (or icon) to present for the
      enter key on virtual keyboards. The attribute can be used with form
      controls (such as the value of textarea elements), or in elements in an
      editing host (e.g., using contenteditable attribute). }
    property EnterKeyHint: string read FEnterKeyHint write FEnterKeyHint;
    { Used to transitively export shadow parts from a nested shadow tree into a
      containing light tree. }
    property ExportParts: string read FExportParts write FExportParts;
    { Prevents rendering of given element, while keeping child elements,
      e.g. script elements, active. }
    property Hidden: string read FHidden write FHidden;
    { Often used with CSS to style a specific element. The value of this
      attribute must be unique. }
    property Id: string read FId write FId;
    { A boolean value that makes the browser disregard user input events for the
      element. Useful when click events are present. }
    property Inert: Boolean read FInert write FInert;
    { Provides a hint as to the type of data that might be entered by the user
      while editing the element or its contents. The attribute can be used with
      form controls (such as the value of textarea elements), or in elements in
      an editing host (e.g., using contenteditable attribute). }
    property InputMode: string read FInputMode write FInputMode;
    { Allows you to specify that a standard HTML element should behave like a
      registered custom built-in element (see Using custom elements for more
      details). }
    property IsCustom: string read FIsCustom write FIsCustom;
    { The unique, global identifier of an item. }
    property ItemId: string read FItemId write FItemId;
    { Used to add properties to an item. Every HTML element may have an itemprop
      attribute specified, where an itemprop consists of a name and value
      pair. }
    property ItemProp: string read FItemProp write FItemProp;
    { Properties that are not descendants of an element with the itemscope
      attribute can be associated with the item using an itemref. It provides a
      list of element ids (not itemids) with additional properties elsewhere in
      the document. }
    property ItemRef: string read FItemRef write FItemRef;
    { Itemscope (usually) works along with itemtype to specify that the HTML
      contained in a block is about a particular item. itemscope creates the
      Item and defines the scope of the itemtype associated with it. itemtype is
      a valid URL of a vocabulary (such as schema.org) that describes the item
      and its properties context. }
    property ItemScope: Boolean read FItemScope write FItemScope;
    { Specifies the URL of the vocabulary that will be used to define itemprops
      (item properties) in the data structure. itemscope is used to set the
      scope of where in the data structure the vocabulary set by itemtype will
      be active. }
    property ItemType: string read FItemType write FItemType;
    { Defines the language used in the element. }
    property Lang: string read FLang write FLang;
    { A cryptographic nonce ("number used once") which can be used by
      Content Security Policy to determine whether or not a given fetch will be
      allowed to proceed. }
    property NOnce: string read FNOnce write FNOnce;
    { Script to be run on abort. }
    property OnAbort: string read FOnAbort write FOnAbort;
    {  }
    property OnAutocomplete: string read FOnAutocomplete write FOnAutocomplete;
    {  }
    property OnAutocompleteError: string read FOnAutocompleteError
      write FOnAutocompleteError;
    { Fires the moment that the element loses focus. }
    property OnBlur: string read FOnBlur write FOnBlur;
    {  }
    property OnCancel: string read FOnCancel write FOnCancel;
    { Script to be run when a file is ready to start playing (when it has
      buffered enough to begin). }
    property OnCanPlay: string read FOnCanPlay write FOnCanPlay;
    { Script to be run when a file can be played all the way to the end without
      pausing for buffering. }
    property OnCanPlaythrough: string read FOnCanPlaythrough
      write FOnCanPlaythrough;
    { Fires the moment when the value of the element is changed. }
    property OnChange: string read FOnChange write FOnChange;
    { Fires on a mouse click on the element. }
    property OnClick: string read FOnClick write FOnClick;
    {  }
    property OnClose: string read FOnClose write FOnClose;
    { Script to be run when a context menu is triggered. }
    property OnContextMenu: string read FOnContextMenu write FOnContextMenu;
    { Script to be run when the cue changes in a <track> element. }
    property OnCueChange: string read FOnCueChange write FOnCueChange;
    { Fires on a mouse double-click on the element. }
    property OnDblClick: string read FOnDblClick write FOnDblClick;
    { Script to be run when an element is dragged. }
    property OnDrag: string read FOnDrag write FOnDrag;
    { Script to be run at the end of a drag operation. }
    property OnDragEnd: string read FOnDragend write FOnDragend;
    { Script to be run when an element has been dragged to a valid drop
      target. }
    property OnDragEnter: string read FOnDragEnter write FOnDragEnter;
    { Script to be run when an element leaves a valid drop target. }
    property OnDragLeave: string read FOnDragLeave write FOnDragLeave;
    { Script to be run when an element is being dragged over a valid drop
      target. }
    property OnDragOver: string read FOnDragOver write FOnDragOver;
    { Script to be run at the start of a drag operation. }
    property OnDragStart: string read FOnDragStart write FOnDragStart;
    { Script to be run when dragged element is being dropped. }
    property OnDrop: string read FOnDrop write FOnDrop;
    { Script to be run when the length of the media changes. }
    property OnDurationChange: string read FOnDurationChange
      write FOnDurationChange;
    { Script to be run when something bad happens and the file is suddenly
      unavailable (like unexpectedly disconnects). }
    property OnEmptied: string read FOnEmptied write FOnEmptied;
    { Script to be run when the media has reach the end (a useful event for
      messages like "thanks for listening"). }
    property OnEnded: string read FOnEnded write FOnEnded;
    { Script to be run when an error occurs when the file is being loaded. }
    property OnError: string read FOnError write FOnError;
    { Fires the moment when the element gets focus. }
    property OnFocus: string read FOnFocus write FOnFocus;
    { Script to be run when an element gets user input. }
    property OnInput: string read FOnInput write FOnInput;
    { Script to be run when an element is invalid. }
    property OnInvalid: string read FOnInvalid write FOnInvalid;
    { Fires when a user is pressing a key. }
    property OnKeyDown: string read FOnKeyDown write FOnKeyDown;
    { Fires when a user presses a key. }
    property OnKeyPress: string read FOnKeyPress write FOnKeyPress;
    { Fires when a user releases a key. }
    property OnKeyUp: string read FOnKeyUp write FOnKeyUp;
    { Fires after the page is finished loading. }
    property OnLoad: string read FOnLoad write FOnLoad;
    { Script to be run when media data is loaded. }
    property OnLoadedData: string read FOnLoadedData write FOnLoadedData;
    { Script to be run when meta data (like dimensions and duration) are
      loaded. }
    property OnLoadedMetaData: string read FOnLoadedMetaData
      write FOnLoadedMetaData;
    { Script to be run just as the file begins to load before anything is
      actually loaded }
    property OnLoadStart: string read FOnLoadStart write FOnLoadStart;
    { Fires when a mouse button is pressed down on an element. }
    property OnMouseDown: string read FOnMouseDown write FOnMouseDown;
    {  }
    property OnMouseEnter: string read FOnMouseEnter write FOnMouseEnter;
    {  }
    property OnMouseLeave: string read FOnMouseLeave write FOnMouseLeave;
    { Fires when the mouse pointer is moving while it is over an element. }
    property OnMouseMove: string read FOnMouseMove write FOnMouseMove;
    { Fires when the mouse pointer moves out of an element. }
    property OnMouseOut: string read FOnMouseOut write FOnMouseOut;
    { Fires when the mouse pointer moves over an element. }
    property OnMouseOver: string read FOnMouseOver write FOnMouseOver;
    { Fires when a mouse button is released over an element. }
    property OnMouseUp: string read FOnMouseUp write FOnMouseUp;
    { Deprecated. Use the onwheel property instead. }
    property OnMouseWheel: string read FOnMouseWheel write FOnMouseWheel;
    { Script to be run when the media is paused either by the user or
      programmatically. }
    property OnPause: string read FOnPause write FOnPause;
    { Script to be run when the media is ready to start playing. }
    property OnPlay: string read FOnPlay write FOnPlay;
    { Script to be run when the media actually has started playing. }
    property OnPlaying: string read FOnPlaying write FOnPlaying;
    { Script to be run when the browser is in the process of getting the media
      data. }
    property OnProgress: string read FOnProgress write FOnProgress;
    { Script to be run each time the playback rate changes (like when a user
      switches to a slow motion or fast forward mode). }
    property OnRateChange: string read FOnRateChange write FOnRateChange;
    { Fires when the Reset button in a form is clicked. }
    property OnReset: string read FOnReset write FOnReset;
    { Fires when the browser window is resized. }
    property OnResize: string read FOnResize write FOnResize;
    { Script to be run when an element's scrollbar is being scrolled. }
    property OnScroll: string read FOnScroll write FOnScroll;
    { Script to be run when the seeking attribute is set to false indicating
      that seeking has ended. }
    property OnSeeked: string read FOnSeeked write FOnSeeked;
    { Script to be run when the seeking attribute is set to true indicating that
      seeking is active. }
    property OnSeeking: string read FOnSeeking write FOnSeeking;
    { Fires after some text has been selected in an element. }
    property OnSelect: string read FOnSelect write FOnSelect;
    {  }
    property OnShow: string read FOnShow write FOnShow;
    {  }
    property OnSort: string read FOnSort write FOnSort;
    { Script to be run when the browser is unable to fetch the media data for
      whatever reason. }
    property OnStalled: string read FOnStalled write FOnStalled;
    { Fires when a form is submitted. }
    property OnSubmit: string read FOnSubmit write FOnSubmit;
    { Script to be run when fetching the media data is stopped before it is
      completely loaded for whatever reason. }
    property OnSuspend: string read FOnSuspend write FOnSuspend;
    { Script to be run when the playing position has changed (like when the user
      fast forwards to a different point in the media). }
    property OnTimeUpdate: string read FOnTimeUpdate write FOnTimeUpdate;
    { Fires when the user opens or closes the <details> element. }
    property OnToggle: string read FOnToggle write FOnToggle;
    { Script to be run each time the volume is changed which (includes setting
      the volume to "mute"). }
    property OnVolumeChange: string read FOnVolumeChange write FOnVolumeChange;
    { Script to be run when the media has paused but is expected to resume
      (like when the media pauses to buffer more data). }
    property OnWaiting: string read FOnWaiting write FOnWaiting;
    { A space-separated list of the part names of the element. Part names allow
      CSS to select and style specific elements in a shadow tree via the ::part
      pseudo-element. }
    property Part: string read FPart write FPart;
    { Used to designate an element as a popover element (see Popover API).
      Popover elements are hidden via display: none until opened via an
      invoking/control element (i.e. a <button> or <input type="button"> with a
      popovertarget attribute) or a HTMLElement.showPopover() call. }
    property Popover: string read FPopover write FPopover;
    { Roles define the semantic meaning of content, allowing screen readers and
      other tools to present and support interaction with an object in a way
      that is consistent with user expectations of that type of object.
      Roles are added to HTML elements using role="role_type", where role_type
      is the name of a role in the ARIA specification. }
    property Role: string read FRole write FRole;
    { Assigns a slot in a shadow DOM shadow tree to an element: An element with
      a slot attribute is assigned to the slot created by the <slot> element
      whose name attribute's value matches that slot attribute's value. }
    property Slot: string read FSlot write FSlot;
    { An enumerated attribute defines whether the element may be checked for
      spelling errors. It may have the following values: empty string or true,
      which indicates that the element should be, if possible, checked for
      spelling errors; false, which indicates that the element should not be
      checked for spelling errors. }
    property SpellCheck: string read FSpellCheck write FSpellCheck;
    { Contains CSS styling declarations to be applied to the element.
      Note that it is recommended for styles to be defined in a separate file or
      files. This attribute and the <style> element have mainly the purpose of
      allowing for quick styling, for example for testing purposes. }
    property Style: string read FStyle write FStyle;
    { An integer attribute indicating if the element can take input focus
      (is focusable), if it should participate to sequential keyboard
      navigation, and if so, at what position. It can take several values:
      a negative value means that the element should be focusable, but should
      not be reachable via sequential keyboard navigation; 0 means that the
      element should be focusable and reachable via sequential keyboard
      navigation, but its relative order is defined by the platform convention;
      a positive value means that the element should be focusable and reachable
      via sequential keyboard navigation; the order in which the elements are
      focused is the increasing value of the tabindex. If several elements share
      the same tabindex, their relative order follows their relative positions
      in the document. }
    property TabIndex: string read FTabIndex write FTabIndex;
    { Contains a text representing advisory information related to the element
      it belongs to. Such information can typically, but not necessarily, be
      presented to the user as a tooltip. }
    property Title: string read FTitle write FTitle;
    { An enumerated attribute that is used to specify whether an element's
      attribute values and the values of its Text node children are to be
      translated when the page is localised, or whether to leave them unchanged.
      It can have the following values: empty string or yes, which indicates
      that the element will be translated. no, which indicates that the element
      will not be translated. }
    property Translate: string read FTranslate write FTranslate;
    { An enumerated attribute used to control the on-screen virtual keyboard
      behaviour on devices such as tablets, mobile phones, or other devices
      where a hardware keyboard may not be available for elements that its
      content is editable (for example, it is an <input> or <textarea> element,
      or an element with the contenteditable attribute set). auto or an empty
      string, which automatically shows the virtual keyboard when the element is
      focused or tapped. manual, which decouples focus and tap on the element
      from the virtual keyboard's state. }
    property VirtualKeyboardPolicy: string read FVirtualKeyboardPolicy
      write FVirtualKeyboardPolicy;
    { This is inherited from the XHTML specifications and deprecated, but kept
      for compatibility purposes. }
    property XmlBase: string read FXmlBase write FXmlBase;
    { This is inherited from the XHTML specifications and deprecated, but kept
      for compatibility purposes. }
    property XmlLang: string read FXmlLang write FXmlLang;
    { This is inherited from the XHTML specifications and deprecated, but kept
      for compatibility purposes. }
    property XmlNs: string read FXmlNs write FXmlNs;
  end;
  { Together with its href attribute, creates a hyperlink to web pages, files,
    email addresses, locations within the current page, or anything else a URL
    can address. }
  TAHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDownload: string;
      FHref: string;
      FHrefLang: string;
      FMedia: string;
      FPing: string;
      FReferrerPolicy: string;
      FRel: string;
      FShape: string;
      FTarget: string;
  published
    { Indicates that the hyperlink is to be used for downloading a resource. }
    property Download: string read FDownload write FDownload;
    { The URL of a linked resource. }
    property Href: string read FHref write FHref;
    { Specifies the language of the linked resource. }
    property HrefLang: string read FHrefLang write FHrefLang;
    { Specifies a hint of the media for which the linked resource was
      designed. }
    property Media: string read FMedia write FMedia;
    { The ping attribute specifies a space-separated list of URLs to be notified
      if a user follows the hyperlink. }
    property Ping: string read FPing write FPing;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { Specifies the relationship of the target object to the link object. }
    property Rel: string read FRel write FRel;
    { The shape of the hyperlink's region in an image map. }
    property Shape: string read FShape write FShape;
    { Specifies where to open the linked document (in the case of an <a>
      element) or where to display the response received (in the case of
      a <form> element). }
    property Target: string read FTarget write FTarget;
  end;
  { Represents an abbreviation or acronym. }
  TAbbrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Allows authors to clearly indicate a sequence of characters that compose an
    acronym or abbreviation for a word. }
  TAcronymHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Indicates that the enclosed HTML provides contact information for a person
    or people, or for an organisation. }
  TAddressHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines an area inside an image map that has predefined clickable areas.
    An image map allows geometric areas on an image to be associated with
    hyperlink. }
  TAreaHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlt: string;
      FCoords: string;
      FDownload: string;
      FHref: string;
      FMedia: string;
      FPing: string;
      FReferrerPolicy: string;
      FRel: string;
      FShape: string;
      FTarget: string;
  published
    { Alternative text in case an image can't be displayed. }
    property Alt: string read FAlt write FAlt;
    { A set of values specifying the coordinates of the hot-spot region. }
    property Coords: string read FCoords write FCoords;
    { Indicates that the hyperlink is to be used for downloading a resource. }
    property Download: string read FDownload write FDownload;
    { The URL of a linked resource. }
    property Href: string read FHref write FHref;
    { Specifies a hint of the media for which the linked resource was
      designed. }
    property Media: string read FMedia write FMedia;
    { The ping attribute specifies a space-separated list of URLs to be notified
      if a user follows the hyperlink. }
    property Ping: string read FPing write FPing;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { Specifies the relationship of the target object to the link object. }
    property Rel: string read FRel write FRel;
    { The shape of the associated hot spot. The specifications for HTML defines
      the values rect, which defines a rectangular region; circle, which defines
      a circular region; poly, which defines a polygon; and default, which
      indicates the entire region beyond any defined shapes. }
    property Shape: string read FShape write FShape;
    { Specifies where to open the linked document (in the case of
      an <a> element) or where to display the response received (in the case of
      a <form> element). }
    property Target: string read FTarget write FTarget;
  end;
  { Represents a self-contained composition in a document, page, application, or
    site, which is intended to be independently distributable or
    reusable (e.g., in syndication). Examples include a forum post, a magazine
    or newspaper article, a blog entry, a product card, a user-submitted
    comment, an interactive widget or gadget, or any other independent item of
    content. }
  TArticleHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a portion of a document whose content is only indirectly related
    to the document's main content. Asides are frequently presented as sidebars
    or call-out boxes. }
  TAsideHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to embed sound content in documents. It may contain one or more audio
    sources, represented using the src attribute or the source element:
    the browser will choose the most suitable one. It can also be the
    destination for streamed media, using a MediaStream. }
  TAudioHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAutoplay: Boolean;
      FControls: Boolean;
      FCrossOrigin: string;
      FLoop: Boolean;
      FMuted: Boolean;
      FPreload: string;
      FSrc: string;
  published
    { The audio or video should play as soon as possible. }
    property Autoplay: Boolean read FAutoplay write FAutoplay;
    { Indicates whether the browser should show playback controls to the user. }
    property Controls: Boolean read FControls write FControls;
    { How the element handles cross-origin requests. }
    property CrossOrigin: string read FCrossOrigin write FCrossOrigin;
    { Indicates whether the media should start playing from the start when it's
      finished. }
    property Loop: Boolean read FLoop write FLoop;
    { Indicates whether the audio will be initially silenced on page load. }
    property Muted: Boolean read FMuted write FMuted;
    { Indicates whether the whole resource, parts of it or nothing should be
      preloaded. }
    property Preload: string read FPreload write FPreload;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
  end;
  { Used to draw the reader's attention to the element's contents, which are not
    otherwise granted special importance. This was formerly known as the
    Boldface element, and most browsers still draw the text in boldface.
    However, you should not use <b> for styling text or granting importance.
    If you wish to create boldface text, you should use the CSS font-weight
    property. If you wish to indicate an element is of special importance, you
    should use the strong element. }
  TBHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies the base URL to use for all relative URLs in a document. There can
    be only one such element in a document. }
  TBaseHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FHref: string;
      FTarget: string;
  published
    { The URL of a linked resource. }
    property Href: string read FHref write FHref;
    { Specifies where to open the linked document (in the case of
      an <a> element) or where to display the response received (in the case of
      a <form> element). }
    property Target: string read FTarget write FTarget;
  end;
  { Tells the browser's bidirectional algorithm to treat the text it contains in
    isolation from its surrounding text. It's particularly useful when a website
    dynamically inserts some text and doesn't know the directionality of the
    text being inserted. }
  TBdiHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Overrides the current directionality of text, so that the text within is
    rendered in a different direction. }
  TBdoHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Renders the enclosed text at a font size one level larger than the
    surrounding text (medium becomes large, for example). The size is capped at
    the browser's maximum permitted font size. }
  TBigHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Indicates that the enclosed text is an extended quotation. Usually, this is
    rendered visually by indentation. A URL for the source of the quotation may
    be given using the cite attribute, while a text representation of the source
    can be given using the <cite> element. }
  TBlockQuoteHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FCite: string;
  published
    { Contains a URI which points to the source of the quote or change. }
    property Cite: string read FCite write FCite;
  end;
  { Represents the content of an HTML document. There can be only one such
    element in a document. }
  TBodyHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FBackground: string;
      FBgColor: string;
  published
    { Specifies the URL of an image file. Note: Although browsers and email
      clients may still support this attribute, it is obsolete.
      Use CSS background-image instead. }
    property Background: string read FBackground write FBackground;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
  end;
  { Produces a line break in text (carriage-return). It is useful for writing a
    poem or an address, where the division of lines is significant. }
  TBrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { An interactive element activated by a user with a mouse, keyboard, finger,
    voice command, or other assistive technology. Once activated, it performs an
    action, such as submitting a form or opening a dialogue. }
  TButtonHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDisabled: Boolean;
      FForm: string;
      FFormAction: string;
      FFormEncType: string;
      FFormMethod: string;
      FFormNoValidate: Boolean;
      FFormTarget: string;
      FName: string;
      FType: string;
      FValue: string;
  published
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Indicates the action of the element, overriding the action defined in
      the <form>. }
    property FormAction: string read FFormAction write FFormAction;
    { If the button/input is a submit button (e.g. type="submit"), this
      attribute sets the encoding type to use during form submission.
      If this attribute is specified, it overrides the enctype attribute of the
      button's form owner. }
    property FormEncType: string read FFormEncType write FFormEncType;
    { If the button/input is a submit button (e.g. type="submit"), this
      attribute sets the submission method to use during form submission
      (GET, POST, etc.). If this attribute is specified, it overrides the method
      attribute of the button's form owner. }
    property FormMethod: string read FFormMethod write FFormMethod;
    { If the button/input is a submit button (e.g. type="submit"), this boolean
      attribute specifies that the form is not to be validated when it is
      submitted. If this attribute is specified, it overrides the novalidate
      attribute of the button's form owner. }
    property FormNoValidate: Boolean read FFormNoValidate write FFormNoValidate;
    { If the button/input is a submit button (e.g. type="submit"), this
      attribute specifies the browsing context (for example, tab, window, or
      inline frame) in which to display the response that is received after
      submitting the form. If this attribute is specified, it overrides the
      target attribute of the button's form owner. }
    property FormTarget: string read FFormTarget write FFormTarget;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Container element to use with either the canvas scripting API or the WebGL
    API to draw graphics and animations. }
  TCanvasHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FHeight: string;
      FWidth: string;
  published
    { The height of the coordinate space in CSS pixels. Defaults to 150. }
    property Height: string read FHeight write FHeight;
    { The width of the coordinate space in CSS pixels. Defaults to 300. }
    property Width: string read FWidth write FWidth;
  end;
  { Specifies the caption (or title) of a table. }
  TCaptionHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
  end;
  { Displays its block-level or inline contents centred horizontally within its
    containing element. }
  TCenterHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to mark up the title of a cited creative work. The reference may be in
    an abbreviated form according to context-appropriate conventions related to
    citation metadata. }
  TCiteHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Displays its contents styled in a fashion intended to indicate that the text
    is a short fragment of computer code. By default, the content text is
    displayed using the user agent's default monospace font. }
  TCodeHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines one or more columns in a column group represented by its implicit or
    explicit parent <colgroup> element. The <col> element is only valid as a
    child of a <colgroup> element that has no span attribute defined. }
  TColHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBgColor: string;
      FSpan: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { Specifies the number of consecutive columns the <col> element spans.
      The value must be a positive integer greater than zero. If not present,
      its default value is 1. }
    property Span: string read FSpan write FSpan;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { Defines a group of columns within a table. }
  TColGroupHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBgColor: string;
      FSpan: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { Specifies the horizontal alignment of each column group cell.
      The possible enumerated values are left, center, right, justify, and char.
      When supported, the char value aligns the textual content on the character
      defined in the char attribute and the offset defined by the charoff
      attribute. Note that the descendant <col> elements may override this value
      using their own align attribute. Use the text-align CSS property on
      the <td> and <th> elements instead, as this attribute is deprecated. }
    property Span: string read FSpan write FSpan;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { An obsolete part of the Web Components suite of technologies-was used inside
    of Shadow DOM as an insertion point, and wasn't meant to be used in ordinary
    HTML. It has now been replaced by the <slot> element, which creates a point
    in the DOM at which a shadow DOM can be inserted. Consider using <slot>
    instead. }
  TContentHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Links a given piece of content with a machine-readable translation.
    If the content is time- or date-related, the <time> element must be used. }
  TDataHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FValue: string;
  published
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Contains a set of <option> elements that represent the permissible or
    recommended options available to choose from within other controls. }
  TDataListHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Provides the description, definition, or value for the preceding term (<dt>)
    in a description list (<dl>). }
  TDdHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a range of text that has been deleted from a document.
    This can be used when rendering "track changes" or source code diff
    information, for example. The <ins> element can be used for the opposite
    purpose: to indicate text that has been added to the document. }
  TDelHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FCite: string;
      FDateTime: TDateTime;
  published
    { Contains a URI which points to the source of the quote or change. }
    property Cite: string read FCite write FCite;
    { Indicates the date and time associated with the element. }
    property DateTime: TDateTime read FDateTime write FDateTime;
  end;
  { Creates a disclosure widget in which information is visible only when the
    widget is toggled into an "open" state. A summary or label must be provided
    using the <summary> element. }
  TDetailsHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FOpen: Boolean;
  published
    { Indicates whether the contents are currently visible (in the case of a
      <details> element) or whether the dialogue is active and can be interacted
      with (in the case of a <dialog> element). }
    property Open: Boolean read FOpen write FOpen;
  end;
  { Used to indicate the term being defined within the context of a definition
    phrase or sentence. The ancestor <p> element, the <dt>/<dd> pairing, or the
    nearest section ancestor of the <dfn> element, is considered to be the
    definition of the term. }
  TDfnHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a dialogue box or other interactive component, such as a
    dismissible alert, inspector, or subwindow. }
  TDialogHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FOpen: Boolean;
  published
    { Indicates whether the contents are currently visible (in the case of a
      <details> element) or whether the dialogue is active and can be interacted
      with (in the case of a <dialog> element). }
    property Open: Boolean read FOpen write FOpen;
  end;
  { Container for a directory of files and/or folders, potentially with styles
    and icons applied by the user agent. Do not use this obsolete element;
    instead, you should use the <ul> element for lists, including lists of
    files. }
  TDirHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { The generic container for flow content. It has no effect on the content or
    layout until styled in some way using CSS (e.g., styling is directly applied
    to it, or some kind of layout model like flexbox is applied to its parent
    element). }
  TDivHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a description list. The element encloses a list of groups of
    terms (specified using the <dt> element) and descriptions (provided by <dd>
    elements). Common uses for this element are to implement a glossary or to
    display metadata (a list of key-value pairs). }
  TDlHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies a term in a description or definition list, and as such must be
    used inside a <dl> element. It is usually followed by a <dd> element;
    however, multiple <dt> elements in a row indicate several terms that are all
    defined by the immediate next <dd> element. }
  TDtHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Marks text that has stress emphasis. The <em> element can be nested, with
    each nesting level indicating a greater degree of emphasis. }
  TEmHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Embeds external content at the specified point in the document.
    This content is provided by an external application or other source of
    interactive content such as a browser plug-in. }
  TEmbedHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FHeight: string;
      FSrc: string;
      FType: string;
      FWidth: string;
  published
    { The displayed height of the resource, in CSS pixels.
      This must be an absolute value; percentages are not allowed. }
    property Height: string read FHeight write FHeight;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { The displayed width of the resource, in CSS pixels.
      This must be an absolute value; percentages are not allowed. }
    property Width: string read FWidth write FWidth;
  end;
  { Used to group several controls as well as labels (<label>) within a web
    form. }
  TFieldSetHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDisabled: Boolean;
      FForm: string;
      FName: string;
  published
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
  end;
  { Represents a caption or legend describing the rest of the contents of its
    parent <figure> element. }
  TFigCaptionHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents self-contained content, potentially with an optional caption,
    which is specified using the <figcaption> element. The figure, its caption,
    and its contents are referenced as a single unit. }
  TFigureHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines the font size, colour and face for its content. }
  TFontHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FColor: string;
  published
    { This attribute sets the text colour using either a named colour or
      a colour specified in the hexadecimal #RRGGBB format.
      Note: This is a legacy attribute. Please use the CSS colour property
      instead. }
    property Color: string read FColor write FColor;
  end;
  { Represents a footer for its nearest ancestor sectioning content or
    sectioning root element. A <footer> typically contains information about the
    author of the section, copyright data, or links to related documents. }
  TFooterHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a document section containing interactive controls for submitting
    information. }
  TFormHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAccept: string;
      FAcceptCharset: string;
      FAction: string;
      FAutocomplete: string;
      FEncType: string;
      FMethod: string;
      FName: string;
      FNoValidate: Boolean;
      FTarget: string;
  published
    { List of types the server accepts, typically a file type. }
    property Accept: string read FAccept write FAccept;
    { List of supported charsets. }
    property AcceptCharset: string read FAcceptCharset write FAcceptCharset;
    { The URI of a program that processes the information submitted via the
      form. }
    property Action: string read FAction write FAction;
    { Indicates whether controls in this form can by default have their values
      automatically completed by the browser. }
    property Autocomplete: string read FAutocomplete write FAutocomplete;
    { Defines the content type of the form data when the method is POST. }
    property EncType: string read FEncType write FEncType;
    { Defines which HTTP method to use when submitting the form.
      Can be GET (default) or POST. }
    property Method: string read FMethod write FMethod;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { This attribute indicates that the form shouldn't be validated when
      submitted. }
    property NoValidate: Boolean read FNoValidate write FNoValidate;
    { Specifies where to open the linked document (in the case of an
      <a> element) or where to display the response received (in the case of a
      <form> element). }
    property Target: string read FTarget write FTarget;
  end;
  { Defines a particular area in which another HTML document can be displayed.
    A frame should be used within a <frameset>. }
  TFrameHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to contain <frame> elements. }
  TFrameSetHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the first level of section headings. }
  TH1HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the second level of section headings. }
  TH2HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the third level of section headings. }
  TH3HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the fourth level of section headings. }
  TH4HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the fifth level of section headings. }
  TH5HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the sixth level of section headings. }
  TH6HtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Contains machine-readable information (metadata) about the document,
    like its title, scripts, and style sheets. }
  THeadHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents introductory content, typically a group of introductory or
    navigational aids. It may contain some heading elements but also a logo,
    a search form, an author name, and other elements. }
  THeaderHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a heading grouped with any secondary content, such as
    subheadings, an alternative title, or a tagline. }
  THGroupHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a thematic break between paragraph-level elements: for example,
    a change of scene in a story, or a shift of topic within a section. }
  THrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FColor: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { This attribute sets the text colour using either a named colour or
      a colour specified in the hexadecimal #RRGGBB format. Note: This is a
      legacy attribute. Please use the CSS colour property instead. }
    property Color: string read FColor write FColor;
  end;
  { Represents the root (top-level element) of an HTML document, so it is also
    referred to as the root element. All other elements must be descendants of
    this element. }
  THtmlHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FManifest: string;
  published
    { Specifies the URL of the document's cache manifest.
      Note: This attribute is obsolete, use <link rel="manifest"> instead. }
    property Manifest: string read FManifest write FManifest;
  end;
  { Represents a range of text that is set off from the normal text for some
    reason, such as idiomatic text, technical terms, and taxonomical
    designations, among others. Historically, these have been presented using
    italicised type, which is the original source of the <i> naming of this
    element. }
  TIHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a nested browsing context, embedding another HTML page into the
    current one. }
  TIFrameHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FAllow: string;
      FCsp: string;
      FFrameBorder: string;
      FHeight: string;
      FLoading: string;
      FMarginHeight: string;
      FMarginWidth: string;
      FName: string;
      FReferrerPolicy: string;
      FSandbox: string;
      FScrolling: string;
      FSrc: string;
      FSrcDoc: string;
      FWidth: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Specifies a feature-policy for the iframe. }
    property Allow: string read FAllow write FAllow;
    { Specifies the Content Security Policy that an embedded document must agree
      to enforce upon itself. }
    property Csp: string read FCsp write FCsp;
    { The value 1 (the default) draws a border around this frame. The value 0
      removes the border around this frame, but you should instead use the CSS
      property border to control <iframe> borders. }
    property FrameBorder: string read FFrameBorder write FFrameBorder;
    { The height of the frame in CSS pixels. Default is 150. }
    property Height: string read FHeight write FHeight;
    { Indicates if the element should be loaded lazily (loading="lazy") or
      loaded immediately (loading="eager"). }
    property Loading: string read FLoading write FLoading;
    { The amount of space in pixels between the frame's content and its top and
      bottom borders. }
    property MarginHeight: string read FMarginHeight write FMarginHeight;
    { The amount of space in pixels between the frame's content and its left and
      right borders. }
    property MarginWidth: string read FMarginWidth write FMarginWidth;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { Stops a document loaded in an iframe from using certain features (such as
      submitting forms or opening new windows). }
    property Sandbox: string read FSandbox write FSandbox;
    { Indicates when the browser should provide a scrollbar for the frame:
      auto - Only when the frame's content is larger than its dimensions.
      yes - Always show a scrollbar.
      no - Never show a scrollbar. }
    property Scrolling: string read FScrolling write FScrolling;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { Inline HTML to embed, overriding the src attribute. If a browser does not
      support the srcdoc attribute, it will fall back to the URL in the src
      attribute. }
    property SrcDoc: string read FSrcDoc write FSrcDoc;
    { The width of the frame in CSS pixels. Default is 300. }
    property Width: string read FWidth write FWidth;
  end;
  { An ancient and poorly supported precursor to the <img> element.
    It should not be used. }
  TImageHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Embeds an image into the document. }
  TImgHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FAlt: string;
      FBorder: string;
      FCrossOrigin: string;
      FDecoding: string;
      FHeight: string;
      FIntrinsicSize: string;
      FIsMap: Boolean;
      FLoading: string;
      FReferrerPolicy: string;
      FSizes: string;
      FSrc: string;
      FSrcSet: string;
      FUseMap: string;
      FWidth: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Alternative text in case an image can't be displayed. }
    property Alt: string read FAlt write FAlt;
    { The border width. Note: This is a legacy attribute.
      Please use the CSS border property instead. }
    property Border: string read FBorder write FBorder;
    { How the element handles cross-origin requests }
    property CrossOrigin: string read FCrossOrigin write FCrossOrigin;
    { Indicates the preferred method to decode the image. }
    property Decoding: string read FDecoding write FDecoding;
    { The intrinsic height of the image, in pixels.
      Must be an integer without a unit. }
    property Height: string read FHeight write FHeight;
    { This attribute tells the browser to ignore the actual intrinsic size of
      the image and pretend it's the size specified in the attribute. }
    property IntrinsicSize: string read FIntrinsicSize write FIntrinsicSize;
    { Indicates that the image is part of a server-side image map. }
    property IsMap: Boolean read FIsMap write FIsMap;
    { Indicates if the element should be loaded lazily (loading="lazy") or
      loaded immediately (loading="eager"). }
    property Loading: string read FLoading write FLoading;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { One or more strings separated by commas, indicating a set of source sizes.
      Each source size consists of:
      1. A media condition. This must be omitted for the last item in the list.
      2. A source size value.
      Media Conditions describe properties of the viewport, not of the image.
      For example, (max-height: 500px) 1000px proposes to use a source of 1000px
      width, if the viewport is not higher than 500px. Source size values
      specify the intended display size of the image. User agents use the
      current source size to select one of the sources supplied by the srcset
      attribute, when those sources are described using width (w) descriptors.
      The selected source size affects the intrinsic size of the image
      (the image's display size if no CSS styling is applied). If the srcset
      attribute is absent, or contains no values with a width descriptor, then
      the sizes attribute has no effect. }
    property Sizes: string read FSizes write FSizes;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { One or more responsive image candidates. }
    property SrcSet: string read FSrcSet write FSrcSet;
    { The partial URL (starting with #) of an image map associated with the
      element. Note: You cannot use this attribute if the <img> element is
      inside an <a> or <button> element. }
    property UseMap: string read FUseMap write FUseMap;
    { The intrinsic width of the image in pixels.
      Must be an integer without a unit. }
    property Width: string read FWidth write FWidth;
  end;
  { Used to create interactive controls for web-based forms to accept data from
    the user; a wide variety of types of input data and control widgets are
    available, depending on the device and user agent. The <input> element is
    one of the most powerful and complex in all of HTML due to the sheer number
    of combinations of input types and attributes. }
  TInputHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAccept: string;
      FAlt: string;
      FAutocomplete: string;
      FCapture: string;
      FChecked: Boolean;
      FDirName: string;
      FDisabled: Boolean;
      FForm: string;
      FFormAction: string;
      FFormEncType: string;
      FFormMethod: string;
      FFormNoValidate: Boolean;
      FFormTarget: string;
      FHeight: string;
      FList: string;
      FMax: string;
      FMaxLength: string;
      FMinLength: string;
      FMin: string;
      FMultiple: Boolean;
      FName: string;
      FPattern: string;
      FPlaceholder: string;
      FReadOnly: Boolean;
      FRequired: Boolean;
      FSize: string;
      FSrc: string;
      FStep: string;
      FType: string;
      FUseMap: string;
      FValue: string;
      FWidth: string;
  published
    { List of types the server accepts, typically a file type. }
    property Accept: string read FAccept write FAccept;
    { Alternative text in case an image can't be displayed. }
    property Alt: string read FAlt write FAlt;
    { Indicates whether controls in this form can by default have their values
      automatically completed by the browser. }
    property Autocomplete: string read FAutocomplete write FAutocomplete;
    { From the Media Capture specification, specifies a new file can be
      captured. }
    property Capture: string read FCapture write FCapture;
    { Indicates whether the element should be checked on page load. }
    property Checked: Boolean read FChecked write FChecked;
    { Valid for hidden, text, search, url, tel, and email input types,
      the dirname attribute enables the submission of the directionality of
      the element. When included, the form control will submit with two
      name/value pairs: the first being the name and value, and the second being
      the value of the dirname attribute as the name, with a value of ltr or rtl
      as set by the browser. }
    property DirName: string read FDirName write FDirName;
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Indicates the action of the element, overriding the action defined
      in the <form>. }
    property FormAction: string read FFormAction write FFormAction;
    { If the button/input is a submit button (e.g. type="submit"),
      this attribute sets the encoding type to use during form submission.
      If this attribute is specified, it overrides the enctype attribute of the
      button's form owner. }
    property FormEncType: string read FFormEncType write FFormEncType;
    { If the button/input is a submit button (e.g. type="submit"),
      this attribute sets the submission method to use during form submission
      (GET, POST, etc.). If this attribute is specified, it overrides the method
      attribute of the button's form owner. }
    property FormMethod: string read FFormMethod write FFormMethod;
    { If the button/input is a submit button (e.g. type="submit"), this boolean
      attribute specifies that the form is not to be validated when it is
      submitted. If this attribute is specified, it overrides the novalidate
      attribute of the button's form owner. }
    property FormNoValidate: Boolean read FFormNoValidate write FFormNoValidate;
    { If the button/input is a submit button (e.g. type="submit"), this
      attribute specifies the browsing context (for example, tab, window, or
      inline frame) in which to display the response that is received after
      submitting the form. If this attribute is specified, it overrides
      the target attribute of the button's form owner. }
    property FormTarget: string read FFormTarget write FFormTarget;
    { Valid for the image input button only, the height is the height of the
      image file to display to represent the graphical submit button.
      See the image input type. }
    property Height: string read FHeight write FHeight;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Identifies a list of pre-defined options to suggest to the user. }
    property List: string read FList write FList;
    { Indicates the maximum value allowed. }
    property Max: string read FMax write FMax;
    { Defines the maximum number of characters allowed in the element. }
    property MaxLength: string read FMaxLength write FMaxLength;
    { Defines the minimum number of characters allowed in the element. }
    property MinLength: string read FMinLength write FMinLength;
    { Indicates the minimum value allowed. }
    property Min: string read FMin write FMin;
    { Indicates whether multiple values can be entered in an input of the type
      email or file. }
    property Multiple: Boolean read FMultiple write FMultiple;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Defines a regular expression which the element's value will be validated
      against. }
    property Pattern: string read FPattern write FPattern;
    { Provides a hint to the user of what can be entered in the field. }
    property Placeholder: string read FPlaceholder write FPlaceholder;
    { Indicates whether the element can be edited. }
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    { Indicates whether this element is required to fill out or not. }
    property Required: Boolean read FRequired write FRequired;
    { Defines the width of the element (in pixels). If the element's type
      attribute is text or password then it's the number of characters. }
    property Size: string read FSize write FSize;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { Valid for date, month, week, time, datetime-local, number, and range,
      the step attribute is a number that specifies the granularity that
      the value must adhere to. If not explicitly included: step defaults to 1
      for number and range. Each date/time input type has a default step value
      appropriate for the type; see the individual input pages: date,
      datetime-local, month, time, and week. The value must be a positive
      number-integer or float. }
    property Step: string read FStep write FStep;
    { A hash-name reference to a <map> element; that is a '#' followed by
      the value of a name of a map element. }
    property UseMap: string read FUseMap write FUseMap;
    { Defines a default value which will be displayed in the element on
      page load. }
    property Value: string read FValue write FValue;
    { Valid for the image input button only, the width is the width of the image
      file to display to represent the graphical submit button.
      See the image input type. }
    property Width: string read FWidth write FWidth;
  end;
  { Represents a range of text that has been added to a document.
    You can use the <del> element to similarly represent a range of text that
    has been deleted from the document. }
  TInsHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FCite: string;
      FDateTime: TDateTime;
  published
    { Contains a URI which points to the source of the quote or change. }
    property Cite: string read FCite write FCite;
    { Indicates the date and time associated with the element. }
    property DateTime: TDateTime read FDateTime write FDateTime;
  end;
  { Represents a span of inline text denoting textual user input from
    a keyboard, voice input, or any other text entry device.
    By convention, the user agent defaults to rendering the contents of a <kbd>
    element using its default monospace font, although this is not mandated by
    the HTML standard. }
  TKbdHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a caption for an item in a user interface. }
  TLabelHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FFor: string;
      FForm: string;
  published
    { Describes elements which belongs to this one. }
    property ForId: string read FFor write FFor;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
  end;
  { Represents a caption for the content of its parent <fieldset>. }
  TLegendHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents an item in a list. It must be contained in a parent element:
    an ordered list (<ol>), an unordered list (<ul>), or a menu (<menu>).
    In menus and unordered lists, list items are usually displayed using bullet
    points. In ordered lists, they are usually displayed with an ascending
    counter on the left, such as a number or letter. }
  TLiHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FValue: string;
  published
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Specifies relationships between the current document and an external
    resource. This element is most commonly used to link to CSS but is also used
    to establish site icons (both "favicon" style icons and icons for the home
    screen and apps on mobile devices) among other things. }
  TLinkHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAs: string;
      FCrossOrigin: string;
      FHref: string;
      FHrefLang: string;
      FIntegrity: string;
      FMedia: string;
      FReferrerPolicy: string;
      FRel: string;
      FSizes: string;
      FType: string;
  published
    { Specifies the type of content being loaded by the link. }
    property AsContent: string read FAs write FAs;
    { How the element handles cross-origin requests }
    property CrossOrigin: string read FCrossOrigin write FCrossOrigin;
    { The URL of a linked resource. }
    property Href: string read FHref write FHref;
    { Specifies the language of the linked resource. }
    property HrefLang: string read FHrefLang write FHrefLang;
    { Specifies a Subresource Integrity value that allows browsers to verify
      what they fetch. }
    property Integrity: string read FIntegrity write FIntegrity;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Specifies a hint of the media for which the linked resource was
      designed. }
    property Media: string read FMedia write FMedia;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { Specifies the relationship of the target object to the link object. }
    property Rel: string read FRel write FRel;
    { This attribute defines the sizes of the icons for visual media contained
      in the resource. It must be present only if the rel contains a value of
      icon or a non-standard type such as Apple's apple-touch-icon. It may have
      the following values: 1. any, meaning that the icon can be scaled to any
      size as it is in a vector format, like image/svg+xml. 2. A white-space
      separated list of sizes, each in the format
      <width in pixels>x<height in pixels> or
      <width in pixels>X<height in pixels>.
      Each of these sizes must be contained in the resource. }
    property Sizes: string read FSizes write FSizes;
  end;
  { Represents the dominant content of the body of a document.
    The main content area consists of content that is directly related to or
    expands upon the central topic of a document, or the central functionality
    of an application. }
  TMainHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used with <area> elements to define an image map (a clickable link area). }
  TMapHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FName: string;
  published
    { Name of the element. For example used by the server to identify
      the fields in form submits. }
    property Name: string read FName write FName;
  end;
  { Represents text which is marked or highlighted for reference or notation
    purposes due to the marked passage's relevance in the enclosing context. }
  TMarkHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to insert a scrolling area of text. You can control what happens when
    the text reaches the edges of its content area using its attributes. }
  TMarqueeHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FBgColor: string;
      FLoop: Boolean;
  published
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { Indicates whether the media should start playing from the start when it's
      finished. }
    property Loop: Boolean read FLoop write FLoop;
  end;
  { The top-level element in MathML. Every valid MathML instance must be wrapped
    in it. In addition, you must not nest a second <math> element in another,
    but you can have an arbitrary number of other child elements in it. }
  TMathHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { A semantic alternative to <ul>, but treated by browsers (and exposed through
    the accessibility tree) as no different than <ul>. It represents
    an unordered list of items (which are represented by <li> elements). }
  TMenuHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FType: string;
  published
    { Defines the type of the element. }
    property Kind: string read FType write FType;
  end;
  { Represents a command that a user is able to invoke through a popup menu.
    This includes context menus, as well as menus that might be attached to
    a menu button. }
  TMenuItemHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents metadata that cannot be represented by other HTML meta-related
    elements, like <base>, <link>, <script>, <style> and <title>. }
  TMetaHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FCharSet: string;
      FContent: string;
      FHttpEquiv: string;
      FName: string;
      FProperty: string;
  published
    { Declares the character encoding of the page or script. }
    property CharSet: string read FCharSet write FCharSet;
    { A value associated with http-equiv or name depending on the context. }
    property Content: string read FContent write FContent;
    { Defines a pragma directive. }
    property HttpEquiv: string read FHttpEquiv write FHttpEquiv;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Some social media and other services use non-standard <meta> attribute
      "property" which this property represents. }
    property Key: string read FProperty write FProperty;
  end;
  { Represents either a scalar value within a known range or a fractional
    value. }
  TMeterHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FForm: string;
      FHigh: string;
      FLow: string;
      FMax: string;
      FMin: string;
      FOptimum: string;
      FValue: string;
  published
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Indicates the lower bound of the upper range. }
    property High: string read FHigh write FHigh;
    { Indicates the upper bound of the lower range. }
    property Low: string read FLow write FLow;
    { Indicates the maximum value allowed. }
    property Max: string read FMax write FMax;
    { Indicates the minimum value allowed. }
    property Min: string read FMin write FMin;
    { Indicates the optimal numeric value. }
    property Optimum: string read FOptimum write FOptimum;
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Represents a section of a page whose purpose is to provide navigation links,
    either within the current document or to other documents. Common examples of
    navigation sections are menus, tables of contents, and indexes. }
  TNavHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Prevents the text it contains from automatically wrapping across multiple
    lines, potentially resulting in the user having to scroll horizontally to
    see the entire width of the text. }
  TNoBrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { An obsolete, non-standard way to provide alternative, or "fallback", content
    for browsers that do not support the embed element or do not support
    the type of embedded content an author wishes to use. This element was
    deprecated in HTML 4.01 and above in favor of placing fallback content
    between the opening and closing tags of an <object> element. }
  TNoEmbedHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Provides content to be presented in browsers that don't support (or have
    disabled support for) the <frame> element. Although most commonly-used
    browsers support frames, there are exceptions, including certain special-use
    browsers including some mobile browsers, as well as text-mode browsers. }
  TNoFramesHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines a section of HTML to be inserted if a script type on the page is
    unsupported or if scripting is currently turned off in the browser. }
  TNoScriptHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents an external resource, which can be treated as an image, a nested
    browsing context, or a resource to be handled by a plugin. }
  TObjectHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FBorder: string;
      FData: string;
      FForm: string;
      FHeight: string;
      FName: string;
      FType: string;
      FUseMap: string;
      FWidth: string;
  published
    { The border width. Note: This is a legacy attribute.
      Please use the CSS border property instead. }
    property Border: string read FBorder write FBorder;
    { Specifies the URL of the resource. }
    property Data: string read FData write FData;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { The height of the displayed resource, in CSS pixels. Absolute values only,
      NO percentages. }
    property Height: string read FHeight write FHeight;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { A hash-name reference to a <map> element; that is a '#' followed by
      the value of a name of a map element. }
    property UseMap: string read FUseMap write FUseMap;
    { The width of the display resource, in CSS pixels. Absolute values only,
      NO percentages. }
    property Width: string read FWidth write FWidth;
  end;
  { Represents an ordered list of items - typically rendered as a numbered
    list. }
  TOlHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FReversed: Boolean;
      FStart: string;
      FType: string;
  published
    { Sets the numbering type: a for lowercase letters, A for uppercase letters,
      i for lowercase Roman numerals, I for uppercase Roman numerals,
      1 for numbers (default). The specified type is used for the entire list
      unless a different type attribute is used on an enclosed <li> element. }
    property Kind: string read FType write FType;
    { Indicates whether the list should be displayed in a descending order
      instead of an ascending order. }
    property Reversed: Boolean read FReversed write FReversed;
    { Defines the first number if other than 1. }
    property Start: string read FStart write FStart;
  end;
  { Creates a grouping of options within a <select> element. }
  TOptGroupHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDisabled: Boolean;
      FLabel: string;
  published
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Specifies a user-readable title of the element. }
    property Title: string read FLabel write FLabel;
  end;
  { Used to define an item contained in a select, an <optgroup>, or a <datalist>
    element. As such, <option> can represent menu items in popups and other
    lists of items in an HTML document. }
  TOptionHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDisabled: Boolean;
      FLabel: string;
      FSelected: Boolean;
      FValue: string;
  published
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Defines a value which will be selected on page load. }
    property Selected: Boolean read FSelected write FSelected;
    { Specifies a user-readable title of the element. }
    property Title: string read FLabel write FLabel;
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Container element into which a site or app can inject the results of
    a calculation or the outcome of a user action. }
  TOutputHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FFor: string;
      FForm: string;
      FName: string;
  published
    { Describes elements which belongs to this one. }
    property ForId: string read FFor write FFor;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
  end;
  { Represents a paragraph. Paragraphs are usually represented in visual media
    as blocks of text separated from adjacent blocks by blank lines and/or
    first-line indentation, but HTML paragraphs can be any structural grouping
    of related content, such as images or form fields. }
  TPHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines parameters for an <object> element. }
  TParamHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FName: string;
      FValue: string;
  published
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Defines a default value which will be displayed in the element on page
      load. }
    property Value: string read FValue write FValue;
  end;
  { Contains zero or more <source> elements and one <img> element to offer
    alternative versions of an image for different display/device scenarios. }
  TPictureHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Renders everything following the start tag as raw text, ignoring any
    following HTML. There is no closing tag, since everything after it is
    considered raw text. }
  TPlainTextHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Enables the embedding of another HTML page into the current one to enable
    smoother navigation into new pages. }
  TPortalHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents preformatted text which is to be presented exactly as written in
    the HTML file. The text is typically rendered using a non-proportional, or
    monospaced, font. Whitespace inside this element is displayed as written. }
  TPreHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Displays an indicator showing the completion progress of a task, typically
    displayed as a progress bar. }
  TProgressHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FForm: string;
      FMax: string;
      FValue: string;
  published
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { This attribute describes how much work the task indicated by the progress
      element requires. The max attribute, if present, must have a value greater
      than 0 and be a valid floating point number. The default value is 1. }
    property Max: string read FMax write FMax;
    { This attribute specifies how much of the task that has been completed.
      It must be a valid floating point number between 0 and max, or
      between 0 and 1 if max is omitted. If there is no value attribute,
      the progress bar is indeterminate; this indicates that an activity is
      ongoing with no indication of how long it is expected to take. }
    property Value: string read FValue write FValue;
  end;
  { Indicates that the enclosed text is a short inline quotation. Most modern
    browsers implement this by surrounding the text in quotation marks.
    This element is intended for short quotations that don't require paragraph
    breaks; for long quotations use the <blockquote> element. }
  TQHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FCite: string;
  published
    { Contains a URI which points to the source of the quote or change. }
    property Cite: string read FCite write FCite;
  end;
  { Used to delimit the base text component of a ruby annotation, i.e. the text
    that is being annotated. One <rb> element should wrap each separate atomic
    segment of the base text. }
  TRbHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to provide fall-back parentheses for browsers that do not support
    the display of ruby annotations using the <ruby> element. One <rp> element
    should enclose each of the opening and closing parentheses that wrap
    the <rt> element that contains the annotation's text. }
  TRpHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies the ruby text component of a ruby annotation, which is used to
    provide pronunciation, translation, or transliteration information for
    East Asian typography. The <rt> element must always be contained within
    a <ruby> element. }
  TRtHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Embraces semantic annotations of characters presented in a ruby of <rb>
    elements used inside of <ruby> element. <rb> elements can have both
    pronunciation (<rt>) and semantic (<rtc>) annotations. }
  TRtcHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents small annotations that are rendered above, below, or next to base
    text, usually used for showing the pronunciation of East Asian characters.
    It can also be used for annotating other kinds of text, but this usage is
    less common. }
  TRubyHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Renders text with a strikethrough, or a line through it. Use the <s> element
    to represent things that are no longer relevant or no longer accurate.
    However, <s> is not appropriate when indicating document edits; for that,
    use the <del> and <ins> elements, as appropriate. }
  TSHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to enclose inline text which represents sample (or quoted) output from
    a computer program. Its contents are typically rendered using the browser's
    default monospaced font (such as Courier or Lucida Console). }
  TSampHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Used to embed executable code or data; this is typically used to embed or
    refer to JavaScript code. The <script> element can also be used with other
    languages, such as WebGL's GLSL shader programming language and JSON. }
  TScriptHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAsync: Boolean;
      FCrossOrigin: string;
      FDefer: Boolean;
      FIntegrity: string;
      FLanguage: string;
      FReferrerPolicy: string;
      FSrc: string;
      FType: string;
  published
    { Executes the script asynchronously. }
    property Async: Boolean read FAsync write FAsync;
    { How the element handles cross-origin requests }
    property CrossOrigin: string read FCrossOrigin write FCrossOrigin;
    { Indicates that the script should be executed after the page has been
      parsed. }
    property Defer: Boolean read FDefer write FDefer;
    { Specifies a Subresource Integrity value that allows browsers to verify
      what they fetch. }
    property Integrity: string read FIntegrity write FIntegrity;
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Defines the script language used in the element. }
    property Language: string read FLanguage write FLanguage;
    { Specifies which referrer is sent when fetching the resource. }
    property ReferrerPolicy: string read FReferrerPolicy write FReferrerPolicy;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
  end;
  { Represents a part that contains a set of form controls or other content
    related to performing a search or filtering operation. }
  TSearchHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a generic standalone section of a document, which doesn't have
    a more specific semantic element to represent it. Sections should always
    have a heading, with very few exceptions. }
  TSectionHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a control that provides a menu of options. }
  TSelectHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAutocomplete: string;
      FDisabled: Boolean;
      FForm: string;
      FMultiple: Boolean;
      FName: string;
      FRequired: Boolean;
      FSize: string;
  published
    { Indicates whether controls in this form can by default have their values
      automatically completed by the browser. }
    property Autocomplete: string read FAutocomplete write FAutocomplete;
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Indicates whether multiple values can be entered in an input of the type
      email or file. }
    property Multiple: Boolean read FMultiple write FMultiple;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Indicates whether this element is required to fill out or not. }
    property Required: Boolean read FRequired write FRequired;
    { If the control is presented as a scrolling list box (e.g. when multiple is
      specified), this attribute represents the number of rows in the list that
      should be visible at one time. Browsers are not required to present
      a select element as a scrolled list box. The default value is 0. }
    property Size: string read FSize write FSize;
  end;
  { An obsolete part of the Web Components technology suite that was intended to
    be used as a shadow DOM insertion point. You might have used it if you have
    created multiple shadow roots under a shadow host. Consider using <slot>
    instead. }
  TShadowHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Part of the Web Components technology suite, this element is a placeholder
    inside a web component that you can fill with your own markup, which lets
    you create separate DOM trees and present them together. }
  TSlotHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents side-comments and small print, like copyright and legal text,
    independent of its styled presentation. By default, it renders text within
    it one font size smaller, such as from small to x-small. }
  TSmallHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies multiple media resources for the picture, the audio element, or
    the video element. It is a void element, meaning that it has no content and
    does not have a closing tag. It is commonly used to offer the same media
    content in multiple file formats in order to provide compatibility with
    a broad range of browsers given their differing support for image file
    formats and media file formats. }
  TSourceHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FHeight: string;
      FMedia: string;
      FSizes: string;
      FSrc: string;
      FSrcSet: string;
      FType: string;
      FWidth: string;
  published
    { Specifies the intrinsic height of the image in pixels. Allowed if the
      parent of <source> is a <picture>. Not allowed if the parent is <audio> or
      <video>. The height value must be an integer without any units. }
    property Height: string read FHeight write FHeight;
    { Specifies the MIME media type of the image or other media type, optionally
      including a codecs parameter. }
    property Kind: string read FType write FType;
    { Specifies the media query for the resource's intended media. }
    property Media: string read FMedia write FMedia;
    { Specifies a list of source sizes that describe the final rendered width of
      the image. Allowed if the parent of <source> is <picture>. Not allowed if
      the parent is <audio> or <video>. The list consists of source sizes
      separated by commas. Each source size is media condition-length pair.
      Before laying the page out, the browser uses this information to determine
      which image defined in srcset to display. Note that sizes will take effect
      only if width descriptors are provided with srcset, not pixel density
      descriptors (i.e., 200w should be used instead of 2x). }
    property Sizes: string read FSizes write FSizes;
    { Specifies the URL of the media resource. Required if the parent of
      <source> is <audio> or <video>. Not allowed if the parent is <picture>. }
    property Src: string read FSrc write FSrc;
    { Specifies a comma-separated list of one or more image URLs and their
      descriptors. Required if the parent of <source> is <picture>. Not allowed
      if the parent is <audio> or <video>. The list consists of strings
      separated by commas, indicating a set of possible images for the browser
      o use. Each string is composed of: 1. A URL specifying an image location.
      2. An optional width descriptor-a positive integer directly followed
      by "w", such as 300w. 3. An optional pixel density descriptor-a positive
      floating number directly followed by "x", such as 2x. Each string in the
      list must have either a width descriptor or a pixel density descriptor to
      be valid. These two descriptors should not be used together; only one
      should be used consistently throughout the list. The value of each
      descriptor in the list must be unique. The browser chooses the most
      adequate image to display at a given point of time based on these
      descriptors. If the descriptors are not specified, the default value used
      is 1x. If the sizes attribute is also present, then each string must
      include a width descriptor. If the browser does not support srcset, then
      src will be used for the default image source. }
    property SrcSet: string read FSrcSet write FSrcSet;
    { Specifies the intrinsic width of the image in pixels. Allowed if the
      parent of <source> is a <picture>. Not allowed if the parent is <audio> or
      <video>. The width value must be an integer without any units. }
    property Width: string read FWidth write FWidth;
  end;
  { A generic inline container for phrasing content, which does not inherently
    represent anything. It can be used to group elements for styling purposes
    (using the class or id attributes), or because they share attribute values,
    such as lang. It should be used only when no other semantic element is
    appropriate. <span> is very much like a div element, but div is
    a block-level element whereas a <span> is an inline-level element. }
  TSpanHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Places a strikethrough (horizontal line) over text. }
  TStrikeHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Indicates that its contents have strong importance, seriousness, or urgency.
    Browsers typically render the contents in bold type. }
  TStrongHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Contains style information for a document or part of a document.
    It contains CSS, which is applied to the contents of the document containing
    this element. }
  TStyleHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FMedia: string;
      FScoped: string;
      FType: string;
  published
    { Defines the type of the element. }
    property Kind: string read FType write FType;
    { Specifies a hint of the media for which the linked resource was
      designed. }
    property Media: string read FMedia write FMedia;
    { Non-standard, deprecated. }
    property Scoped: string read FScoped write FScoped;
  end;
  { Specifies inline text which should be displayed as subscript for solely
    typographical reasons. Subscripts are typically rendered with a lowered
    baseline using smaller text. }
  TSubHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies a summary, caption, or legend for a details element's disclosure
    box. Clicking the <summary> element toggles the state of
    the parent <details> element open and closed. }
  TSummaryHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Specifies inline text which is to be displayed as superscript for solely
    typographical reasons. Superscripts are usually rendered with a raised
    baseline using smaller text. }
  TSupHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Container defining a new coordinate system and viewport.
    It is used as the outermost element of SVG documents, but it can also be
    used to embed an SVG fragment inside an SVG or HTML document. }
  TSvgHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents tabular data-that is, information presented in a two-dimensional
    table comprised of rows and columns of cells containing data. }
  TTableHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBackground: string;
      FBgColor: string;
      FBorder: string;
      FCellPadding: string;
      FCellSpacing: string;
      FFrame: string;
      FRules: string;
      FSummary: string;
      FVAlign: string;
      FWidth: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Specifies the URL of an image file. Note: Although browsers and email
      clients may still support this attribute, it is obsolete.
      Use CSS background-image instead. }
    property Background: string read FBackground write FBackground;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The border width. Note: This is a legacy attribute. Please use the CSS
      border property instead. }
    property Border: string read FBorder write FBorder;
    { Defines the space between the content of a cell and its border.
      This attribute is obsolete: instead of using it, apply the padding CSS
      property to the <th> and <td> elements. }
    property CellPadding: string read FCellPadding write FCellPadding;
    { Defines the size of the space between two cells. This attribute is
      obsolete: instead of using it, set the border-spacing CSS property on the
      <table> element. Note that this has no effect if the <table> element's
      border-collapse CSS property is set to collapse. }
    property CellSpacing: string read FCellSpacing write FCellSpacing;
    { Defines which side of the frame surrounding the table must be displayed.
      The possible enumerated values are void, above, below, hsides, vsides,
      lhs, rhs, box and border. Use the border-style and border-width CSS
      properties instead, as this attribute is deprecated. }
    property Frame: string read FFrame write FFrame;
    { Defines where rules (borders) are displayed in the table. The possible
      enumerated values are none (default value), groups (<thead>, <tbody>, and
      <tfoot> elements), rows (horizontal lines), cols (vertical lines), and all
      (border around every cell). Use the border CSS property on the appropriate
      table-related elements, as well as on the <table> itself, instead, as this
      attribute is deprecated. }
    property Rules: string read FRules write FRules;
    { Defines an alternative text that summarizes the content of the table.
      Use the <caption> element instead, as this attribute is deprecated. }
    property Summary: string read FSummary write FSummary;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
    { Specifies the width of the table. Use the width CSS property instead, as
      this attribute is deprecated. }
    property Width: string read FWidth write FWidth;
  end;
  { Encapsulates a set of table rows (<tr> elements), indicating that they
    comprise the body of a table's (main) data. }
  TTBodyHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FBackground: string;
      FBgColor: string;
      FVAlign: string;
  published
    { Specifies the URL of an image file. Note: Although browsers and email
      clients may still support this attribute, it is obsolete. Use CSS
      background-image instead. }
    property Background: string read FBackground write FBackground;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { A child of the <tr> element, it defines a cell of a table that contains
    data. }
  TTdHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBackground: string;
      FBgColor: string;
      FColSpan: string;
      FHeaders: string;
      FRowSpan: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Specifies the URL of an image file. Note: Although browsers and email
      clients may still support this attribute, it is obsolete.
      Use CSS background-image instead. }
    property Background: string read FBackground write FBackground;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The colspan attribute defines the number of columns a cell should span. }
    property ColSpan: string read FColSpan write FColSpan;
    { IDs of the <th> elements which applies to this element. }
    property Headers: string read FHeaders write FHeaders;
    { Defines the number of rows a table cell should span over. }
    property RowSpan: string read FRowSpan write FRowSpan;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { A mechanism for holding HTML that is not to be rendered immediately when
    a page is loaded but may be instantiated subsequently during runtime using
    JavaScript. }
  TTemplateHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a multi-line plain-text editing control, useful when you want to
    allow users to enter a sizeable amount of free-form text, for example,
    a comment on a review or feedback form. }
  TTextAreaHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAutocomplete: string;
      FCols: string;
      FDirName: string;
      FDisabled: Boolean;
      FForm: string;
      FMaxLength: string;
      FMinLength: string;
      FName: string;
      FPlaceholder: string;
      FReadOnly: Boolean;
      FRequired: Boolean;
      FRows: string;
      FWrap: string;
  published
    { Indicates whether controls in this form can by default have their values
      automatically completed by the browser. }
    property Autocomplete: string read FAutocomplete write FAutocomplete;
    { Defines the number of columns in a textarea. }
    property Cols: string read FCols write FCols;
    { This attribute is used to indicate the text directionality of the element
      contents similar to the dirname attribute of the <input> element.
      For more information, see the dirname attribute. }
    property DirName: string read FDirName write FDirName;
    { Indicates whether the user can interact with the element. }
    property Disabled: Boolean read FDisabled write FDisabled;
    { The enterkeyhint specifies what action label (or icon) to present for the
      enter key on virtual keyboards. The attribute can be used with form
      controls (such as the value of textarea elements), or in elements in an
      editing host (e.g., using contenteditable attribute). }
    property EnterKeyHint: string read FEnterKeyHint write FEnterKeyHint;
    { Indicates the form that is the owner of the element. }
    property Form: string read FForm write FForm;
    { Defines the maximum number of characters allowed in the element. }
    property MaxLength: string read FMaxLength write FMaxLength;
    { Defines the minimum number of characters allowed in the element. }
    property MinLength: string read FMinLength write FMinLength;
    { Name of the element. For example used by the server to identify the fields
      in form submits. }
    property Name: string read FName write FName;
    { Provides a hint to the user of what can be entered in the field. }
    property Placeholder: string read FPlaceholder write FPlaceholder;
    { Indicates whether the element can be edited. }
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    { Indicates whether this element is required to fill out or not. }
    property Required: Boolean read FRequired write FRequired;
    { Defines the number of rows in a text area. }
    property Rows: string read FRows write FRows;
    { Indicates whether the text should be wrapped. }
    property Wrap: string read FWrap write FWrap;
  end;
  { Encapsulates a set of table rows (<tr> elements), indicating that they
    comprise the foot of a table with information about the table's columns.
    This is usually a summary of the columns, e.g., a sum of the given numbers
    in a column. }
  TTFootHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBgColor: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { A child of the <tr> element, it defines a cell as the header of a group of
    table cells. The nature of this group can be explicitly defined by the scope
    and headers attributes. }
  TThHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBackground: string;
      FBgColor: string;
      FColSpan: string;
      FHeaders: string;
      FRowSpan: string;
      FScope: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Specifies the URL of an image file. Note: Although browsers and email
      clients may still support this attribute, it is obsolete.
      Use CSS background-image instead. }
    property Background: string read FBackground write FBackground;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The colspan attribute defines the number of columns a cell should span. }
    property ColSpan: string read FColSpan write FColSpan;
    { IDs of the <th> elements which applies to this element. }
    property Headers: string read FHeaders write FHeaders;
    { Defines the number of rows a table cell should span over. }
    property RowSpan: string read FRowSpan write FRowSpan;
    { Defines the cells that the header test (defined in the th element)
      relates to. }
    property Scope: string read FScope write FScope;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { Encapsulates a set of table rows (<tr> elements), indicating that they
    comprise the head of a table with information about the table's columns.
    This is usually in the form of column headers (<th> elements). }
  TTheadHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
  end;
  { Represents a specific period in time. It may include the datetime attribute
    to translate dates into machine-readable format, allowing for better search
    engine results or custom features such as reminders. }
  TTimeHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDateTime: TDateTime;
  published
    { Indicates the date and time associated with the element. }
    property DateTime: TDateTime read FDateTime write FDateTime;
  end;
  { Defines the document's title that is shown in a browser's title bar or
    a page's tab. It only contains text; tags within the element are ignored. }
  TTitleHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Defines a row of cells in a table. The row's cells can then be established
    using a mix of <td> (data cell) and <th> (header cell) elements. }
  TTrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAlign: string;
      FBgColor: string;
      FVAlign: string;
  published
    { Specifies the horizontal alignment of the element. }
    property Align: string read FAlign write FAlign;
    { Background colour of the element. Note: This is a legacy attribute.
      Please use the CSS background-color property instead. }
    property BgColor: string read FBgColor write FBgColor;
    { The purpose of the HTML valign attribute is to define the vertical
      alignment of the content of a table cell.  }
    property VAlign: string read FVAlign write FVAlign;
  end;
  { Used as a child of the media elements, audio and video. It lets you specify
    timed text tracks (or time-based data), for example to automatically handle
    subtitles. The tracks are formatted in
    WebVTT format (.vtt files)-Web Video Text Tracks. }
  TTrackHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FDefault: Boolean;
      FKind: string;
      FLabel: string;
      FSrc: string;
      FSrcLang: string;
  published
    { Indicates that the track should be enabled unless the user's preferences
      indicate something different. }
    property IsDefault: Boolean read FDefault write FDefault;
    { Specifies the kind of text track. }
    property Kind: string read FKind write FKind;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { Language of the track text data. It must be a valid BCP 47 language tag.
      If the kind attribute is set to subtitles, then srclang must be defined. }
    property SrcLang: string read FSrcLang write FSrcLang;
    { Specifies a user-readable title of the element. }
    property Title: string read FLabel write FLabel;
  end;
  { Creates inline text which is presented using the user agent default
    monospace font face. This element was created for the purpose of rendering
    text as it would be displayed on a fixed-width display such as a teletype,
    text-only screen, or line printer. }
  TTtHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents a span of inline text which should be rendered in a way that
    indicates that it has a non-textual annotation. This is rendered by default
    as a simple solid underline but may be altered using CSS. }
  TUHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents an unordered list of items, typically rendered as a bulleted
    list. }
  TUlHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Represents the name of a variable in a mathematical expression or
    a programming context. It's typically presented using an italicised version
    of the current typeface, although that behaviour is browser-dependent. }
  TVarHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Embeds a media player which supports video playback into the document.
    You can also use <video> for audio content, but the audio element may
    provide a more appropriate user experience. }
  TVideoHtmlComponent = class sealed(TNonCustomHtmlComponent)
  private
    var
      FAutoplay: Boolean;
      FControls: Boolean;
      FControlsList: string;
      FCrossOrigin: string;
      FDisablePictureInPicture: Boolean;
      FDisableRemotePlayback: Boolean;
      FHeight: string;
      FLoop: Boolean;
      FMuted: Boolean;
      FPlaysInline: Boolean;
      FPoster: string;
      FPreload: string;
      FSrc: string;
      FWidth: string;
  published
    { The audio or video should play as soon as possible. }
    property Autoplay: Boolean read FAutoplay write FAutoplay;
    { Indicates whether the browser should show playback controls to the user. }
    property Controls: Boolean read FControls write FControls;
    { The controlslist attribute, when specified, helps the browser select what
      controls to show for the video element whenever the browser shows its own
      set of controls (that is, when the controls attribute is specified).
      The allowed values are nodownload, nofullscreen and noremoteplayback.
      Use the disablepictureinpicture attribute if you want to disable the
      Picture-In-Picture mode (and the control). }
    property ControlsList: string read FControLslist write FControlsList;
    { How the element handles cross-origin requests. }
    property CrossOrigin: string read FCrossOrigin write FCrossOrigin;
    { Prevents the browser from suggesting a Picture-in-Picture context menu or
      to request Picture-in-Picture automatically in some cases. }
    property DisablePictureInPicture: Boolean read FDisablepictureInPicture
      write FDisablePictureInPicture;
    { A Boolean attribute used to disable the capability of remote playback in
      devices that are attached using wired (HDMI, DVI, etc.) and wireless
      technologies (Miracast, Chromecast, DLNA, AirPlay, etc.). In Safari, you
      can use x-webkit-airplay="deny" as a fallback. }
    property DisableRemotePlayback: Boolean read FDisableRemotePlayback
      write FDisableRemotePlayback;
    { The height of the video's display area, in CSS pixels
      (absolute values only; no percentages). }
    property Height: string read FHeight write FHeight;
    { Indicates whether the media should start playing from the start when it's
      finished. }
    property Loop: Boolean read FLoop write FLoop;
    { Indicates whether the audio will be initially silenced on page load. }
    property Muted: Boolean read FMuted write FMuted;
    { A Boolean attribute indicating that the video is to be played "inline";
      that is, within the element's playback area. Note that the absence of this
      attribute does not imply that the video will always be played in
      fullscreen. }
    property PlaysInline: Boolean read FPlaysInline write FPlaysInline;
    { A URL indicating a poster frame to show until the user plays or seeks. }
    property Poster: string read FPoster write FPoster;
    { Indicates whether the whole resource, parts of it or nothing should be
      preloaded. }
    property Preload: string read FPreload write FPreload;
    { The URL of the embeddable content. }
    property Src: string read FSrc write FSrc;
    { The width of the video's display area, in CSS pixels
      (absolute values only; no percentages). }
    property Width: string read FWidth write FWidth;
  end;
  { Represents a word break opportunity-a position within text where the browser
    may optionally break a line, though its line-breaking rules would not
    otherwise create a break at that location. }
  TWbrHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;
  { Renders text between the start and end tags without interpreting the HTML in
    between and using a monospaced font. The HTML2 specification recommended
    that it should be rendered wide enough to allow 80 characters per line. }
  TXmpHtmlComponent = class sealed(TNonCustomHtmlComponent)
  end;

implementation

uses
  Classes, StrUtils, SysUtils;

const
  GUnicodeAttrs: array of array [1..2] of UnicodeString = (
    ('accept-charset', 'acceptcharset'), ('as', 'ascontent'),
    ('class', 'cssclass'), ('default', 'isdefault'), ('for', 'forid'),
    ('http-equiv', 'httpequiv'), ('is', 'iscustom'), ('label', 'title'),
    ('property', 'key'), ('type', 'kind'), ('xml:base', 'xmlbase'),
    ('xml:lang', 'xmllang'), ('xml:ns', 'xmlns'));

var
  GAttrPair: array [1..2] of UnicodeString;
  GAttrCache: specialize TDictionary<string, string>;

class function THtmlComponentFoundation.ClassOfDomNode(Node: TDomNode): TClass;
var
  NodeClassName: string;
begin
  with Node do
    if NodeType = ELEMENT_NODE then
    begin
      NodeClassName := 'T' + StringsReplace(string(NodeName), ['-', ':'],
        ['', ''], [rfReplaceAll]);
      if not TryClassNamed(NodeClassName + 'HtmlComponent', Result) then
        Result := ClassNamed(NodeClassName)
    end
    else
      Result := inherited
end;

procedure THtmlComponentFoundation.SetProperty(const Prop, Template: string);
var
  Attr: string;
  Key: string;
begin
  Key := Prop.ToLower.TrimRight([':']);
  if GAttrCache.TryGetValue(Key, Attr) then
    inherited SetProperty(Attr, Template)
  else
    inherited SetProperty(Key, Template)
end;

class function THtmlComponent.DomName: string;
begin
  Result := ClassName.Replace('HtmlComponent', '')
end;

procedure THtmlComponent.Render(Dom: TDomNode);
var
  Owned: TComponent;
begin
  if AgIf then for Owned in Self do (Owned as TFoundation).Render(Dom)
end;

class function TNonCustomHtmlComponent.DomName: string;
begin
  Result := ClassName.Substring(1, ClassName.Length - 14)
end;

procedure TNonCustomHtmlComponent.Render(Dom: TDomNode);
var
  Attr: array [1..2] of UnicodeString;
begin
  if AgIf then
  begin
    inherited;
    with TDomElement(Dom.LastChild) do
    begin
      if HasAttribute('tag') then RemoveAttribute('tag');
      for Attr in GUnicodeAttrs do
      begin
        if HasAttribute(Attr[2]) then
        begin
          SetAttribute(Attr[1], GetAttribute(Attr[2]));
          RemoveAttribute(Attr[2])
        end
      end
    end
  end
end;

initialization

GAttrCache := specialize TDictionary<string, string>.Create;
for GAttrPair in GUnicodeAttrs do
  GAttrCache.AddOrSetValue(string(GAttrPair[1]), string(GAttrPair[2]));

RegisterClasses([TAgBlockHtmlComponent, TAHtmlComponent, TAbbrHtmlComponent,
  TAcronymHtmlComponent, TAddressHtmlComponent, TAreaHtmlComponent,
  TArticleHtmlComponent, TAsideHtmlComponent, TAudioHtmlComponent,
  TBHtmlComponent, TBaseHtmlComponent, TBdiHtmlComponent, TBdoHtmlComponent,
  TBigHtmlComponent, TBlockQuoteHtmlComponent, TBodyHtmlComponent,
  TBrHtmlComponent, TButtonHtmlComponent, TCanvasHtmlComponent,
  TCaptionHtmlComponent, TCenterHtmlComponent, TCiteHtmlComponent,
  TCodeHtmlComponent, TColHtmlComponent, TColGroupHtmlComponent,
  TContentHtmlComponent, TDataHtmlComponent, TDataListHtmlComponent,
  TDdHtmlComponent, TDelHtmlComponent, TDetailsHtmlComponent, TDfnHtmlComponent,
  TDialogHtmlComponent, TDirHtmlComponent, TDivHtmlComponent, TDlHtmlComponent,
  TDtHtmlComponent, TEmHtmlComponent, TEmbedHtmlComponent,
  TFieldSetHtmlComponent, TFigCaptionHtmlComponent, TFigureHtmlComponent,
  TFontHtmlComponent, TFooterHtmlComponent, TFormHtmlComponent,
  TFrameHtmlComponent, TFrameSetHtmlComponent, TH1HtmlComponent,
  TH2HtmlComponent, TH3HtmlComponent, TH4HtmlComponent, TH5HtmlComponent,
  TH6HtmlComponent, THeadHtmlComponent, THeaderHtmlComponent,
  THGroupHtmlComponent, THrHtmlComponent, THtmlHtmlComponent, TIHtmlComponent,
  TIFrameHtmlComponent, TImageHtmlComponent, TImgHtmlComponent,
  TInputHtmlComponent, TInsHtmlComponent, TKbdHtmlComponent,
  TLabelHtmlComponent, TLegendHtmlComponent, TLiHtmlComponent,
  TLinkHtmlComponent, TMainHtmlComponent, TMapHtmlComponent, TMarkHtmlComponent,
  TMarqueeHtmlComponent, TMathHtmlComponent, TMenuHtmlComponent,
  TMenuItemHtmlComponent, TMetaHtmlComponent, TMeterHtmlComponent,
  TNavHtmlComponent, TNoBrHtmlComponent, TNoEmbedHtmlComponent,
  TNoFramesHtmlComponent, TNoScriptHtmlComponent, TObjectHtmlComponent,
  TOlHtmlComponent, TOptGroupHtmlComponent, TOptionHtmlComponent,
  TOutputHtmlComponent, TPHtmlComponent, TParamHtmlComponent,
  TPictureHtmlComponent, TPlainTextHtmlComponent, TPortalHtmlComponent,
  TPreHtmlComponent, TProgressHtmlComponent, TQHtmlComponent, TRbHtmlComponent,
  TRpHtmlComponent, TRtHtmlComponent, TRtcHtmlComponent, TRubyHtmlComponent,
  TSHtmlComponent, TSampHtmlComponent, TScriptHtmlComponent,
  TSearchHtmlComponent, TSectionHtmlComponent, TSelectHtmlComponent,
  TShadowHtmlComponent, TSlotHtmlComponent, TSmallHtmlComponent,
  TSourceHtmlComponent, TSpanHtmlComponent, TStrikeHtmlComponent,
  TStrongHtmlComponent, TStyleHtmlComponent, TSubHtmlComponent,
  TSummaryHtmlComponent, TSupHtmlComponent, TSvgHtmlComponent,
  TTableHtmlComponent, TTBodyHtmlComponent, TTdHtmlComponent,
  TTemplateHtmlComponent, TTextAreaHtmlComponent, TTFootHtmlComponent,
  TThHtmlComponent, TTHeadHtmlComponent, TTimeHtmlComponent,
  TTitleHtmlComponent, TTrHtmlComponent, TTrackHtmlComponent, TTtHtmlComponent,
  TUHtmlComponent, TUlHtmlComponent, TVarHtmlComponent, TVideoHtmlComponent,
  TWbrHtmlComponent, TXmpHtmlComponent])

finalization

GAttrCache.Free

end.
