unit Argentum.Arc;

{$I FPC}

interface

type
  { Tells how the object wrapped by a TArc should be initialised. }
  TArcInit = (acCreate, acNil);
  { Provides automatic lifetime management for the set object via automatic
    reference counting. }
  generic TArc<T: class; const Init: TArcInit> = record
  private
    type
      TDefaultConstructible = class(T)
      public
        constructor Create; reintroduce;
      end;
      IHolder = interface
        function GetObject: T;
        procedure SetObject(Obj: T);
      end;
      THolder = class(TInterfacedObject, IHolder)
      private
        var
          FObject: T;
      public
        constructor Create(Obj: T = nil);
        destructor Destroy; override;
        function GetObject: T; inline;
        procedure SetObject(Obj: T); inline;
      end;
    var
      FHolder: IHolder;
    function GetObject: T; inline;
  public
    { Assigns the object of which lifetime should be automatically managed by
      this wrapper.
      @param(Obj is the object of which lifetime should be automatically managed
      by this wrapper.) }
    class operator :=(Obj: T): TArc; inline;
    { Provides access to the managed object. }
    property _: T read GetObject;
  end;

implementation

constructor TArc.TDefaultConstructible.Create;
begin
  inherited
end;

constructor TArc.THolder.Create(Obj: T);
begin
  inherited Create;
  if not Assigned(Obj) then
    case Init of
      acCreate: FObject := TDefaultConstructible.Create;
      acNil: FObject := nil
      otherwise;
    end
end;

destructor TArc.THolder.Destroy;
begin
  FObject.Free;
  inherited
end;

function TArc.THolder.GetObject: T;
begin
  Result := FObject
end;

procedure TArc.THolder.SetObject(Obj: T);
begin
  FObject := Obj
end;

function TArc.GetObject: T;
begin
  if not Assigned(FHolder) then FHolder := THolder.Create;
  Result := FHolder.GetObject
end;

class operator TArc.:=(Obj: T): TArc;
begin
  Result.FHolder := THolder.Create(Obj)
end;

end.
